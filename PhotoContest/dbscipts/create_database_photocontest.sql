DROP SCHEMA IF EXISTS `photocontest`;
create database if not exists photocontest;
use photocontest;

create or replace table users
(
    user_id    int auto_increment
        primary key,
    username   varchar(20) null,
    password   text        not null,
    first_name varchar(30) not null,
    last_name  varchar(20) not null,
    email      varchar(30) not null,
    scores     int         null,
    picture    text        null,
    constraint users_username_uindex
        unique (username)
);

create or replace table ranks
(
    rank_id   int auto_increment
        primary key,
    rank_name varchar(15) not null
);

create or replace table user_ranks
(
    user_id int not null,
    rank_id int not null,
    constraint user_roles_roles_fk
        foreign key (rank_id) references ranks (rank_id),
    constraint user_roles_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table category
(
    category_id   int auto_increment
        primary key,
    category_name varchar(30) not null
);

create or replace table contest_phases
(
    contest_phases_id int auto_increment
        primary key,
    phase_name        varchar(20) not null
);

create or replace table comments
(
    comment_id    int auto_increment,
    comment_value text null,
    constraint comments_pk
        primary key (comment_id)
);

create or replace table contest_types
(
    type_id   int auto_increment
        primary key,
    type_name varchar(20) not null,
    constraint contest_types_type_name_uindex
        unique (type_name)
);

create or replace table reviews
(
    review_id  int auto_increment,
    user_id    int null,
    comment_id int null,
    points     int null,
    constraint review_pk
        primary key (review_id),
    constraint comments_comments_fk
        foreign key (comment_id) references comments (comment_id),
    constraint users_user_fk
        foreign key (user_id) references users (user_id)
);

create or replace table images
(
    img_id       int auto_increment
        primary key,
    title        varchar(50) not null,
    story        text        not null,
    img_url      text        not null,
    winner_place int         not null
);

create or replace table contests
(
    contest_id       int auto_increment
        primary key,
    title            varchar(50)          not null,
    contest_phase_id int                  not null,
    start_date       datetime             null,
    finish_date      datetime             null,
    from_hour        datetime             null,
    to_hour          datetime             null,
    category_id      int                  not null,
    type_id          int                  not null,
    cover_photo      int                  not null,
    assessed         tinyint(1) default 0 not null,
    constraint contests__phases_fk
        foreign key (contest_phase_id) references contest_phases (contest_phases_id),
    constraint contests_categories_fk
        foreign key (category_id) references category (category_id),
    constraint contests_types_fk
        foreign key (type_id) references contest_types (type_id),
    constraint contests_images_fk
        foreign key (cover_photo) references images (img_id),
    constraint images_title_uindex
        unique (title)
);

create or replace table images_reviews
(
    image_id  int not null,
    review_id int not null,
    constraint images_reviews_images_fk
        foreign key (image_id) references images (img_id),
    constraint images_reviews_reviews_fk
        foreign key (review_id) references reviews (review_id)
);


create or replace table contest_users
(
    user_id    int not null,
    contest_id int null,
    constraint contest_users_contest_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contest_users_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table users_images
(
    user_id  int not null,
    image_id int not null,
    constraint uses_images_images_fk
        foreign key (image_id) references images (img_id),
    constraint uses_images_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table images_contest
(
    img_id     int not null,
    contest_id int not null,
    constraint images_contest_contest_fk
        foreign key (contest_id) references photocontest.contests (contest_id),
    constraint images_contest_images_fk
        foreign key (img_id) references photocontest.images (img_id)
);

create or replace table contest_jury
(
    contest_id int not null,
    user_id    int not null,
    constraint contest_jury_contests_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contest_jury_users_fk
        foreign key (user_id) references users (user_id)
);