package com.photocontest;

import com.photocontest.models.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestsHelpers {


    public static Category createMockCategory() {
        Category mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("Nature");
        return mockCategory;
    }


    public static Contest createMockContest() throws ParseException {
        Contest mockContest = new Contest();
        mockContest.setId(1);
        mockContest.setTitle("Secret Waterfall");
        mockContest.setPhase(createMockPhase());
        mockContest.setStartDate(new SimpleDateFormat("yyyy-MM-dd").parse("27-04-21"));
        mockContest.setFinishDate(new SimpleDateFormat("yyyy-MM-dd").parse("27-05-21"));
        mockContest.setPhase2FromHour(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("28-05-21 00:01"));
        mockContest.setPhase2ToHour(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("28-05-21 23:59"));
        mockContest.setCategory(createMockCategory());
        mockContest.setType(createMockType());
        mockContest.setCoverPhoto(createMockImage());
        mockContest.setAssessed(true);

        return mockContest;
    }

    public static Phase createMockPhase() {
        Phase mockPhase = new Phase();
        mockPhase.setId(1);
        mockPhase.setName("Phase 1");
        return mockPhase;
    }

    public static Type createMockType() {
        Type mockType = new Type();
        mockType.setId(1);
        mockType.setName("Invitational");
        return mockType;
    }

    public static Image createMockImage(){
        Image mockImage = new Image();
        mockImage.setId(1);
        mockImage.setTitle("Anghel");
        mockImage.setStory("It has a cumulative height of 979m");
        mockImage.setImageUrl("https://www.amazingplaces.com/wp-content/uploads/2016/12/Angel-Falls-venezuela.jpg");
        mockImage.setWinnerPlace(1);
        return mockImage;
    }

    public static User createMockUser() {
        User mockUser = new User();
        mockUser.setId(1);
        mockUser.setUsername("jaro");
        mockUser.setFirstName("Petar");
        mockUser.setLastName("Tudjarov");
        mockUser.setEmail("petar@yahoo.com");
        mockUser.setScores(1600);
        mockUser.setPicture("https://datasciencelab.unimi.it/wp-content/uploads/2017/06/user.png");
        mockUser.setPassword("1234");
        return mockUser;
    }

    public static Review createMockReview(){
        Review mockReview = new Review();
        mockReview.setId(1);
        mockReview.setUser(createMockUser());
        mockReview.setComment(createMockComment());
        mockReview.setPoints(150);
        return mockReview;
    }

    public static Comment createMockComment(){
        Comment mockComment = new Comment();
        mockComment.setId(1);
        mockComment.setValue("This is mock comment");
        return mockComment;
    }

}
