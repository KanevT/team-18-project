package com.photocontest.service;

import com.photocontest.models.Image;
import com.photocontest.models.Rank;
import com.photocontest.models.Review;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.ImageRepository;
import com.photocontest.repositories.contracts.ReviewRepository;
import com.photocontest.services.ReviewServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static com.photocontest.TestsHelpers.*;

@ExtendWith(MockitoExtension.class)
public class ReviewServiceTest {

    private static final String MOCK_RANK_ADMIN = "Admin";

    @Mock
    ReviewRepository mockRepository;
    @Mock
    ImageRepository mockImageRepository;

    @InjectMocks
    ReviewServiceImpl reviewService;

    Review mockReview;
    User mockUser;
    Image mockImage;

    @BeforeEach
    public void setup() {
        mockReview = createMockReview();
        mockUser = createMockUser();
        mockUser.setRanks(Set.of(new Rank(1, MOCK_RANK_ADMIN)));
        mockImage = createMockImage();
    }

    @Test
    public void getAll_Should_CallRepository_When_TryToGetAllRanks() {
        //Act
        reviewService.getAll(mockReview);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_ReturnRank_When_IdMatched() {
        // Arrange
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockReview);

        //Act
        Review result = reviewService.getById(1, mockUser);

        // Assert
        Assertions.assertEquals(mockReview.getId(), result.getId());
        Assertions.assertEquals(mockReview.getPoints(), result.getPoints());
    }

    @Test
    public void create_Should_CallRepository_When_UpValidParameter() {
        //Act
        reviewService.create(mockReview, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockReview);
    }

    @Test
    public void update_Should_CallRepository_When_UpValidParameter() {
        //Act
        reviewService.update(mockReview, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockReview);
    }

    @Test
    public void addReviewToImage_Should_CallRepository_When_ValidParameter() {
        //Act
        reviewService.addReviewToImage(mockReview, mockImage);

        // Assert
        Mockito.verify(mockImageRepository, Mockito.times(1))
                .update(mockImage);
    }


}
