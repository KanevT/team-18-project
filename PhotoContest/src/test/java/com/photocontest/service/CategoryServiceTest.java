package com.photocontest.service;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Category;
import com.photocontest.repositories.contracts.CategoryRepository;
import com.photocontest.services.CategoryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.photocontest.TestsHelpers.createMockCategory;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

    private static final String MOCK_CATEGORY_NATURE = "Nature";

    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl service;

    Category mockCategory;
    List<Category> citiesList;

    @BeforeEach
    public void setup() {
        mockCategory = createMockCategory();
        citiesList = new ArrayList<>();
        citiesList.add(mockCategory);
    }

    @Test
    public void getAll_Should_ReturnCategoryList_When_CallRepository() {
        // Arrange
        Mockito.when(service.getAll()).thenReturn(citiesList);

        //Act
        List<Category> result = service.getAll();

        // Assert
        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void getById_Should_ReturnCategory_When_Matched() {
        // Arrange
        Mockito.when(service.getById(Mockito.anyInt())).thenReturn(mockCategory);

        //Act
        Category result = service.getById(3);

        // Assert
        Assertions.assertEquals(mockCategory.getId(), result.getId());
        Assertions.assertEquals(mockCategory.getName(), result.getName());
    }

    @Test
    public void getByName_Should_ReturnCategory_When_ValidParameter() {
        // Arrange
        Mockito.when(mockRepository.getByName(MOCK_CATEGORY_NATURE)).thenReturn(mockCategory);

        //Act
        Category result = service.getByName(Optional.of(MOCK_CATEGORY_NATURE));

        // Assert
        Assertions.assertEquals(mockCategory.getName(), result.getName());
    }

    @Test
    public void getByName_Should_Throw_When_MethodReturnNull() {
        // Arrange
        Mockito.when(mockRepository.getByName(Mockito.anyString())).thenReturn(null);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getByName(Optional.of(MOCK_CATEGORY_NATURE)));
    }


}
