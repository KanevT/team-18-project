package com.photocontest.service;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Rank;
import com.photocontest.repositories.contracts.RankRepository;
import com.photocontest.services.RankServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class RankServiceTest {

    private static final String MOCK_RANK_ADMIN = "Admin";
    @Mock
    RankRepository mockRepository;

    @InjectMocks
    RankServiceImpl rankService;

    Rank mockRank;

    @BeforeEach
    public void setup() {
        mockRank = new Rank(1, MOCK_RANK_ADMIN);
    }

    @Test
    public void getAll_Should_CallRepository_When_TryToGetAllRanks() {
        // Act
        rankService.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_Return_Rank_When_IdMatched() {
        // Arrange
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockRank);

        //Act
        Rank result = rankService.getById(1);

        // Assert
        Assertions.assertEquals(mockRank.getId(), result.getId());
        Assertions.assertEquals(mockRank.getName(), result.getName());
    }

    @Test
    public void getById_Should_Throw_When_GetBuIdReturnNull() {
        // Arrange
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(null);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> rankService.getById(Mockito.anyInt()));
    }

    @Test
    public void getByName_Should_ReturnRank_When_NameMatched() {
        // Arrange
        Mockito.when(mockRepository.getByName(Mockito.anyString())).thenReturn(mockRank);

        //Act
        Rank result = rankService.getByName(Optional.of(Mockito.anyString()));

        // Assert
        Assertions.assertEquals(mockRank.getId(), result.getId());
        Assertions.assertEquals(mockRank.getName(), result.getName());
    }

    @Test
    public void getByName_Should_Throw_When_NameNotFound() {
        // Arrange
        Mockito.when(mockRepository.getByName(Mockito.anyString())).thenReturn(null);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> rankService.getByName(Optional.of(Mockito.anyString())));
    }

}
