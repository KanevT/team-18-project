package com.photocontest.service;

import com.photocontest.models.Image;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.ImageRepository;
import com.photocontest.services.ImageServiceImpl;
import okhttp3.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import static com.photocontest.TestsHelpers.createMockImage;
import static com.photocontest.TestsHelpers.createMockUser;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ImageServiceTest {

    private static final String MOCK_CATEGORY_NATURE = "Nature";

    @Mock
    ImageRepository mockRepository;

    @InjectMocks
    ImageServiceImpl imageService;

    Image mockImage;
    User mockUser;
    List<Image> images;

    @BeforeEach
    public void setup() {
        mockImage = createMockImage();
        mockUser = createMockUser();
        images = new ArrayList<>();
        images.add(mockImage);
    }

    @Test
    public void getAll_Should_CallRepository_When_MethodIsUse() {
        //Act
        imageService.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_ReturnImage_When_Matched() {
        // Arrange
        Mockito.when(imageService.getById(Mockito.anyInt())).thenReturn(mockImage);

        //Act
        Image result = imageService.getById(1);

        // Assert
        Assertions.assertEquals(mockImage.getId(), result.getId());
        Assertions.assertEquals(mockImage.getTitle(), result.getTitle());
    }

    @Test
    public void create_Should_CallRepository_When_CreateImage() {
        //Act
        imageService.create(mockImage);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockImage);
    }

    @Test
    public void searchByMultiply_Should_ReturnListImages_When_ValidParameter() {
        // Arrange
        Mockito.when(mockRepository.searchByTitleOrStory(Optional.of(MOCK_CATEGORY_NATURE),
                (Optional.of(MOCK_CATEGORY_NATURE))))
                .thenReturn(images);

        //Act
        List<Image> result = imageService.searchByMultiply(Optional.of(MOCK_CATEGORY_NATURE),
                Optional.of(MOCK_CATEGORY_NATURE));

        // Assert
        Assertions.assertEquals(mockImage.getTitle(), result.get(0).getTitle());
    }

    @Test
    public void getWinnerImages_Should_CallRepository_When_GetWinnerImages() {
        //Act
        imageService.getWinnerImages();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getWinnerImages();
    }


    @Test
    public void uploadImageToImgurAPI_Should_CallRepository_When_GetWinnerImages() {
        //Act
        imageService.getWinnerImages();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getWinnerImages();
    }
}
