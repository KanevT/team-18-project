package com.photocontest.service;

import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.Contest;
import com.photocontest.models.DTO.UpdatePasswordDto;
import com.photocontest.models.Image;
import com.photocontest.models.Rank;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.UserRepository;
import com.photocontest.services.UserServiceImpl;
import com.photocontest.services.contracts.ContestService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.photocontest.TestsHelpers.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    private static final int MOCK_USER_ID = 1;
    private static final int TESTED_METHOD_ID = 2;
    private static final String USER_RANK_MASTER = "Master";
    private static final String USER_RANK_ADMIN = "Admin";
    private static final String TEST_PASSWORD = "7878";
    private static final String PHASE_NAME_PHASE_1 = "Phase 1";
    private static final String PHASE_NAME_FINISHED = "Finished";

    @Mock
    UserRepository mockRepository;
    @Mock
    PasswordEncoder mockPasswordEncoder;
    @Mock
    ContestService mockContestService;

    @InjectMocks
    UserServiceImpl userService;

    User mockUser;
    UpdatePasswordDto mockUpdatePasswordDto;
    Contest mockContest;
    List<Contest> mockContests;
    Image mockImage;

    @BeforeEach
    public void setup() throws ParseException {
        mockUser = createMockUser();
        mockUser.setRanks(Set.of(new Rank(1, USER_RANK_ADMIN)));
        mockUpdatePasswordDto = new UpdatePasswordDto();
        mockContest = createMockContest();
        mockContests = new ArrayList<>();
        mockContests.add(mockContest);
        mockImage = createMockImage();
    }

    @Test
    public void getAll_Should_CallRepository_When_TryToGetAllRanks() {
        // Act
        userService.getAll(mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getByUsername_Should_Return_User_When_UsernameMatched() {
        // Arrange
        Mockito.when(mockRepository.getByUsername((Mockito.anyString()))).thenReturn(mockUser);

        // Act
        User result = userService.getByUsername(Mockito.anyString());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getByFirstName_Should_Return_User_When_FirstNameMatched() {
        // Arrange
        Mockito.when(mockRepository.getByFirsName((Mockito.anyString()))).thenReturn(mockUser);

        // Act
        User result = userService.getByFirstName(Mockito.anyString());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getByLastName_Should_Return_User_When_LastNameMatched() {
        // Arrange
        Mockito.when(mockRepository.getByLastName((Mockito.anyString()))).thenReturn(mockUser);

        // Act
        User result = userService.getByLastName(Mockito.anyString());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getByEmaile_Should_Return_User_When_EmailMatched() {
        // Arrange
        Mockito.when(mockRepository.getByEmail((Mockito.anyString()))).thenReturn(mockUser);

        // Act
        User result = userService.getByEmail(Mockito.anyString());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getById_Should_Return_User_When_IdMatched() {
        // Arrange
        Mockito.when(mockRepository.getById((Mockito.anyInt()))).thenReturn(mockUser);

        // Act
        User result = userService.getById(Mockito.anyInt(), mockUser);

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getById_Should_Throw_When_UserIsUnauthorized() {
        // Arrange
        mockUser.setRanks(Set.of(new Rank(MOCK_USER_ID, USER_RANK_MASTER)));

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.getById(TESTED_METHOD_ID, mockUser));
    }

    @Test
    public void create_Should_CallRepository_When_ValidParameter() {
        // Arrange
        Mockito.when(mockRepository.getByUsername(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        userService.create(mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    public void create_Should_Thrown_When_UserExist() {
        // Arrange
        Mockito.when(mockRepository.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(mockUser));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsAdmin() {
        // Arrange
        User actionUser = createMockUser();
        Mockito.when(mockRepository.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);

        Mockito.when(mockRepository.getByUsername(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        userService.update(mockUser, actionUser);

        // Assert
        Mockito.verify(mockRepository, times(1))
                .update(actionUser);
    }

    @Test
    public void update_Should_Thrown_WhenUserAlreadyExist() {
        // Arrange
        User actionUser = createMockUser();
        actionUser.setId(2);
        Mockito.when(mockRepository.getByUsername(Mockito.anyString()))
                .thenReturn(actionUser);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.update(mockUser, actionUser));
    }

    @Test
    public void update_Should_Thrown_WhenUserIsAnonymous() {
        // Arrange
        Mockito.when(mockRepository.getByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);

        User actionUser = createMockUser();
        actionUser.setId(4);
        mockUser.setRanks(Set.of(new Rank(2, USER_RANK_MASTER)));

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.update(mockUser, actionUser));
    }

    @Test
    public void delete_Should_Thrown_When_UserIsNotFound() {
        // Arrange
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.delete(mockUser.getId(), mockUser));
    }

    @Test
    public void delete_Should_Thrown_When_UserIsNotEmployee() {
        // Arrange
        mockUser.setRanks(Set.of(new Rank(3, USER_RANK_MASTER)));

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.delete(Mockito.anyInt(), mockUser));
    }

    @Test
    public void delete_Should_Call_Repository_WhenUserIsEmployee() {
        // Arrange
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        // Act
        userService.delete(mockUser.getId(), mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockUser.getId());
    }

    @Test
    public void search_By_Multiple_Should_Thrown_When_UserIsNotAuthorized() {
        // Act, Assert
        mockUser.setRanks(Set.of(new Rank(2, USER_RANK_MASTER)));
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.searchByMultiple(Optional.empty(),
                        (Optional.empty()),
                        (Optional.empty()),
                        (Optional.empty()),
                        mockUser));
    }


    @Test
    public void search_By_Multiple_Should_CallRepository_When_UserIsAdmin() {
        // Act
        userService.searchByMultiple(Optional.empty(),
                (Optional.empty()),
                (Optional.empty()),
                (Optional.empty()),
                mockUser);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .searchByMultiple(Optional.empty(),
                        (Optional.empty()),
                        (Optional.empty()),
                        (Optional.empty()));
    }

    @Test
    public void searchByOneWord_Should_CallRepository_When_UserIsAdmin() {
        // Act
        userService.searchByOneWord(Optional.empty(),
                mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .searchByOneWord(Optional.empty());
    }

    @Test
    public void getCustomerCount_Should_CallRepository_When_UseMethod() {
        // Act
        userService.getCustomerCount();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getCustomerCount();
    }

    @Test
    public void addRank_Should_ReturnRank_When_ValidParameter() {
        // Arrange
        Rank mockRank = new Rank(2, USER_RANK_MASTER);

        // Act
        Rank result = userService.addRank(createMockUser(), mockRank);

        // Assert
        Assertions.assertEquals(result.getId(), mockRank.getId());
        Assertions.assertEquals(result.getName(), mockRank.getName());

    }

    @Test
    public void getUserContest_Should_Return_AllContestsOfSomeUser() {
        // Arrange
        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUser);
        // Act
        Set<Contest> result = userService.getUserContests(Mockito.anyInt(), mockUser);

        // Assert
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void getUserContest_Should_Throw_When_UserIsUnauthorized() {
        // Arrange
        mockUser.setRanks(Set.of(new Rank(2, USER_RANK_MASTER)));
        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.getUserContests(Mockito.anyInt(), mockUser));
    }

    @Test
    public void passwordUpdate_Should_CallRepository_When_ValidParameter() {
        // Arrange
        UpdatePasswordDto mockUpdatePasswordDto = new UpdatePasswordDto();
        mockUpdatePasswordDto.setNewPassword(TEST_PASSWORD);
        Mockito.when(mockPasswordEncoder.encode(TEST_PASSWORD))
                .thenReturn(TEST_PASSWORD);

        // Act
        userService.passwordUpdate(mockUpdatePasswordDto, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

//    @Test
//    public void passwordUpdate_Should_Throw_When_PasswordIsNotCorrect() {
//        // Arrange
//        UpdatePasswordDto mockUpdatePasswordDto = new UpdatePasswordDto();
//        mockUpdatePasswordDto.setOldPassword("2323");
//        CharSequence charSequence = "test";
//
//        Mockito.when(mockPasswordEncoder.matches(charSequence,Mockito.anyString() ))
//                .thenReturn(false);
//
//        // Act, Assert
//        Assertions.assertThrows(AuthorisationException.class,
//                () -> userService.passwordUpdate(mockUpdatePasswordDto,mockUser));
//    }


    @Test
    public void getUserActiveContests_Should_ReturnActiveContest_When_ValidParameter() {
        // Arrange
        mockUser.setContests(Set.of(mockContest));
        Mockito.when(mockContestService.getByPhase(Optional.of(PHASE_NAME_PHASE_1)))
                .thenReturn(mockContests);

        // Act
        List<Contest> result = userService.getUserActiveContests(mockUser);

        // Assert
        Assertions.assertEquals(result.get(0).getId(), mockContest.getId());
        Assertions.assertEquals(result.get(0).getTitle(), mockContest.getTitle());
    }

    @Test
    public void getUserFinishedContests_Should_ReturnFinishedContest_When_ValidParameter() {
        // Arrange
        mockUser.setContests(Set.of(mockContest));

        Mockito.when(mockContestService.getByPhase(Optional.of(PHASE_NAME_FINISHED)))
                .thenReturn(mockContests);

        // Act
        List<Contest> result = userService.getUserFinishedContests(mockUser);

        // Assert
        Assertions.assertEquals(result.get(0).getId(), mockContest.getId());
        Assertions.assertEquals(result.get(0).getTitle(), mockContest.getTitle());
    }

    @Test
    public void addImageToUser_Should_ReturnImage_When_ValidParameter() {
        // Act
        Image result = userService.addImageToUser(mockUser, mockImage);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }


}
