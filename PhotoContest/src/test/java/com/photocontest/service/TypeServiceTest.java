package com.photocontest.service;

import com.photocontest.models.Type;
import com.photocontest.repositories.contracts.TypeRepository;
import com.photocontest.services.TypeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.photocontest.TestsHelpers.createMockType;

@ExtendWith(MockitoExtension.class)
public class TypeServiceTest {

    @Mock
    TypeRepository mockRepository;

    @InjectMocks
    TypeServiceImpl typeService;

    Type mockType;

    @BeforeEach
    public void setup() {
        mockType = createMockType();
    }

    @Test
    public void getAll_Should_CallRepository_When_TryToGetAllType() {
        // Act
        typeService.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_ReturnType_When_IdMatched() {
        // Arrange
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockType);

        // Act
        Type result = typeService.getById(1);

        // Assert
        Assertions.assertEquals(mockType.getId(), result.getId());
        Assertions.assertEquals(mockType.getName(), result.getName());
    }

    @Test
    public void getByName_Should_ReturnRank_When_NameMatched() {
        // Arrange
        Mockito.when(mockRepository.getByName(Mockito.anyString())).thenReturn(mockType);

        //Act
        Type result = typeService.getByName(Mockito.anyString());

        // Assert
        Assertions.assertEquals(mockType.getId(), result.getId());
        Assertions.assertEquals(mockType.getName(), result.getName());
    }


}
