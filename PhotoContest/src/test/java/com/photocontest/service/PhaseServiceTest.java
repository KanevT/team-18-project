package com.photocontest.service;

import com.photocontest.models.Phase;
import com.photocontest.repositories.contracts.PhaseRepository;
import com.photocontest.services.PhaseServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.photocontest.TestsHelpers.createMockPhase;

@ExtendWith(MockitoExtension.class)
public class PhaseServiceTest {

    @Mock
    PhaseRepository mockRepository;

    @InjectMocks
    PhaseServiceImpl phaseService;

    Phase mockPhase;

    @BeforeEach
    public void setup() {
        mockPhase = createMockPhase();
    }

    @Test
    public void getAll_Should_CallRepository() {
        //Act
        phaseService.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_ReturnPhase_When_IdMatched() {
        // Arrange
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockPhase);

        //Act
        Phase result = phaseService.getById(1);

        // Assert
        Assertions.assertEquals(mockPhase.getId(), result.getId());
        Assertions.assertEquals(mockPhase.getName(), result.getName());
    }

    @Test
    public void getName_Should_ReturnPhase_When_NameMatched() {
        // Arrange
        Mockito.when(mockRepository.getByName(Mockito.anyString())).thenReturn(mockPhase);

        //Act
        Phase result = phaseService.getByName(Mockito.anyString());

        // Assert
        Assertions.assertEquals(mockPhase.getId(), result.getId());
        Assertions.assertEquals(mockPhase.getName(), result.getName());
    }


}
