package com.photocontest.service;

import com.photocontest.models.*;
import com.photocontest.models.DTO.ContestDto;
import com.photocontest.repositories.contracts.*;
import com.photocontest.services.ScheduledService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;

import static com.photocontest.TestsHelpers.*;

@ExtendWith(MockitoExtension.class)
public class ScheduledServiceTest {

    private static final int MOCK_ID_1 = 1;
    private static final String MOCK_RANK_ADMIN = "Admin";
    private static final String USER_RANK_MASTER = "Master";
    private static final String USER_RANK_ENTHUSIAST = "Enthusiast";
    private static final String USER_RANK_JUNKIE = "Junkie";
    private static final String USER_RANK_PHOTO_DICTATOR = "Photo Dictator";

    @Mock
    ContestRepository mockContestRepository;
    @Mock
    UserRepository mockUserRepository;
    @Mock
    ImageRepository mockImageRepository;
    @Mock
    RankRepository mockRankRepository;
    @Mock
    PhaseRepository mockPhaseRepository;


    @InjectMocks
    ScheduledService scheduledService;

    User mockUser;
    Contest mockContest;
    Image mockImage;
    Image mockImage2;
    Image mockImage3;
    Image mockImage4;
    Image mockImage5;
    Image mockImage6;
    Image mockImage7;
    Review mockReview;
    ContestDto mockContestDto;
    Phase mockPhase;

    @BeforeEach
    public void setup() throws ParseException {
        mockUser = createMockUser();
        mockContest = createMockContest();
        mockUser.setRanks(Set.of(new Rank(MOCK_ID_1, MOCK_RANK_ADMIN)));
        mockImage = createMockImage();
        mockReview = createMockReview();
        mockContestDto = new ContestDto();
        mockPhase = createMockPhase();
        mockImage2 = new Image(2, "New", "Same Story", "https://www.amazingplaces.com/wp-content/uploads/2016/12/Angel-Falls-venezuela.jpg", 0,
                Set.of(new Review(2, createMockUser(), createMockComment(), 300)), List.of(mockUser));
        mockImage3 = new Image(3, "New3", "Same Story", "https://www.amazingplaces.com/wp-content/uploads/2016/12/Angel-Falls-venezuela.jpg", 0,
                Set.of(new Review(3, createMockUser(), createMockComment(), 22)), List.of(mockUser));
        mockImage4 = new Image(4, "New4", "Same Story", "https://www.amazingplaces.com/wp-content/uploads/2016/12/Angel-Falls-venezuela.jpg", 0,
                Set.of(new Review(4, createMockUser(), createMockComment(), 22)), List.of(mockUser));
        mockImage5 = new Image(5, "New5", "Same Story", "https://www.amazingplaces.com/wp-content/uploads/2016/12/Angel-Falls-venezuela.jpg", 0,
                Set.of(new Review(5, createMockUser(), createMockComment(), 11)), List.of(mockUser));
        mockImage6 = new Image(6, "New6", "Same Story", "https://www.amazingplaces.com/wp-content/uploads/2016/12/Angel-Falls-venezuela.jpg", 0,
                Set.of(new Review(6, createMockUser(), createMockComment(), 11)), List.of(mockUser));
        mockImage7 = new Image(7, "New7", "Same Story", "https://www.amazingplaces.com/wp-content/uploads/2016/12/Angel-Falls-venezuela.jpg", 0,
                Set.of(new Review(7, createMockUser(), createMockComment(), 10)), List.of(mockUser));
    }

    @Test
    public void run_Should_WorkProperly_When_IsCalled() {

        mockUser.setRanks(Set.of(new Rank(4, USER_RANK_MASTER)));
        mockUser.setScores(1872);

        Mockito.when(mockContestRepository.getAll())
                .thenReturn(List.of(mockContest));

        Mockito.when(mockUserRepository.getAll())
                .thenReturn(List.of(mockUser));

        Mockito.when(mockContestRepository.getByPhase(Mockito.anyString()))
                .thenReturn(List.of(mockContest));

        Mockito.when(mockRankRepository.getByName(USER_RANK_MASTER))
                .thenReturn(new Rank(4, USER_RANK_MASTER));

        Mockito.when(mockRankRepository.getByName(USER_RANK_PHOTO_DICTATOR))
                .thenReturn(new Rank(5, USER_RANK_PHOTO_DICTATOR));

        scheduledService.run();

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void checkPhaseForContest_Should_UpdateContest_When_IsTrue() {
        List<Contest> contestList = List.of(mockContest);

        Mockito.when(mockPhaseRepository.getById(Mockito.anyInt()))
                .thenReturn(new Phase(3, "Phase 2"));

        scheduledService.checkPhaseForContest(contestList);

        Mockito.verify(mockContestRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    public void checkRank_Should_UpdateSomeUser_When_Scores_IsGreaterThenDefault_And_Rank_IsJunkie() {
        mockUser.setRanks(Set.of(new Rank(2, USER_RANK_JUNKIE)));
        mockUser.setScores(123);
        List<User> userList = List.of(mockUser);

        Mockito.when(mockRankRepository.getByName(USER_RANK_MASTER))
                .thenReturn(new Rank(4, USER_RANK_MASTER));

        Mockito.when(mockRankRepository.getByName(USER_RANK_ENTHUSIAST))
                .thenReturn(new Rank(3, USER_RANK_ENTHUSIAST));

        Mockito.when(mockRankRepository.getByName(USER_RANK_JUNKIE))
                .thenReturn(new Rank(2, USER_RANK_JUNKIE));

        scheduledService.checkRank(userList);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void checkRank_Should_UpdateSomeUser_When_Scores_IsGreaterThenDefault_And_Rank_IsMaster() {
        mockUser.setRanks(Set.of(new Rank(4, USER_RANK_MASTER)));
        mockUser.setScores(1872);
        List<User> userList = List.of(mockUser);

        Mockito.when(mockRankRepository.getByName(USER_RANK_MASTER))
                .thenReturn(new Rank(4, USER_RANK_MASTER));

        Mockito.when(mockRankRepository.getByName(USER_RANK_PHOTO_DICTATOR))
                .thenReturn(new Rank(5, USER_RANK_PHOTO_DICTATOR));

        scheduledService.checkRank(userList);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void checkRank_Should_UpdateSomeUser_When_Scores_IsGreaterThenDefault_And_Rank_IsEnthusiast() {
        mockUser.setRanks(Set.of(new Rank(3, USER_RANK_ENTHUSIAST)));
        mockUser.setScores(180);
        List<User> userList = List.of(mockUser);

        Mockito.when(mockRankRepository.getByName(USER_RANK_MASTER))
                .thenReturn(new Rank(4, USER_RANK_MASTER));

        Mockito.when(mockRankRepository.getByName(USER_RANK_ENTHUSIAST))
                .thenReturn(new Rank(3, USER_RANK_ENTHUSIAST));


        scheduledService.checkRank(userList);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void calculateAndRewardPoints_Should_PassCheckForMoreThenOnePlace_When_IsHaveOnlyOneImage() {
        mockContest.setAssessed(false);
        mockContest.setImages(Set.of(mockImage));
        mockImage.setUsers(List.of(mockUser));

        List<Contest> contestList = List.of(mockContest);

        Mockito.when(mockImageRepository.getById(Mockito.anyInt()))
                .thenReturn(mockImage);

        scheduledService.calculateAndRewardPoints(contestList);

        Mockito.verify(mockImageRepository, Mockito.times(1))
                .update(mockImage);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);

        Mockito.verify(mockContestRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    public void calculateAndRewardPoints_Should_UpdateContest_When_FirstWithDoubleScoreThenSecond() {
        mockContest.setAssessed(false);
        mockImage.setReviews(Set.of(mockReview));

        mockContest.setImages(Set.of(mockImage, mockImage2));
        mockImage.setUsers(List.of(mockUser));

        List<Contest> contestList = List.of(mockContest);

        Mockito.when(mockImageRepository.getById(Mockito.anyInt()))
                .thenReturn(mockImage);

        scheduledService.calculateAndRewardPoints(contestList);

        Mockito.verify(mockImageRepository, Mockito.times(1))
                .update(mockImage);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);

        Mockito.verify(mockContestRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    public void calculateAndRewardPoints_Should_UpdateContest_When_HaveFirstAndSecondPlace() {
        mockContest.setAssessed(false);
        mockImage.setReviews(Set.of(mockReview));

        mockContest.setImages(Set.of(mockImage, mockImage3, mockImage4, mockImage5));
        mockImage.setUsers(List.of(mockUser));

        List<Contest> contestList = List.of(mockContest);

        Mockito.when(mockImageRepository.getById(Mockito.anyInt()))
                .thenReturn(mockImage);

        scheduledService.calculateAndRewardPoints(contestList);

        Mockito.verify(mockImageRepository, Mockito.times(4))
                .update(mockImage);

        Mockito.verify(mockUserRepository, Mockito.times(4))
                .update(mockUser);

        Mockito.verify(mockContestRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    public void calculateAndRewardPoints_Should_UpdateContest_When_HaveFirstSecondAndThirdPlace() {
        mockContest.setAssessed(false);
        mockImage.setReviews(Set.of(mockReview));

        mockContest.setImages(Set.of(mockImage, mockImage3, mockImage4, mockImage5, mockImage6, mockImage7));
        mockImage.setUsers(List.of(mockUser));

        List<Contest> contestList = List.of(mockContest);

        Mockito.when(mockImageRepository.getById(Mockito.anyInt()))
                .thenReturn(mockImage);

        scheduledService.calculateAndRewardPoints(contestList);

        Mockito.verify(mockImageRepository, Mockito.times(5))
                .update(mockImage);

        Mockito.verify(mockUserRepository, Mockito.times(5))
                .update(mockUser);

        Mockito.verify(mockContestRepository, Mockito.times(1))
                .update(mockContest);
    }
}
