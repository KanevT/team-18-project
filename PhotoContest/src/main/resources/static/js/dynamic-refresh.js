$(document).keypress(
    function(event){
        debugger
        if (event.keyIdentifier==='U+000A'||event.keyIdentifier==='Enter'||event.keyCode===13) {
            event.preventDefault();
        }
    });

let searchElement = $('.search-change');
let searchElementHome = $('.search-change-home');
let countryDropdown = $('.country-dropdown');
searchElement.change(getSearchResults);
searchElement.keyup(getSearchResults);
countryDropdown.change(getBreweries);
searchElementHome.keyup(getSearchResultsHome);


searchElementHome.change(getSearchResultsHome);

function getDifferentPages(event, value) {
    debugger
    event.preventDefault();
    let isUserOrBeer = $(value).hasClass("users");
    const url = $(value).attr('href');

    isUserOrBeer ? $('.user-results').load(url) : $('.beer-results').load(url);
}

function getSearchResults(event) {
    debugger
    event.preventDefault();
    let isUserOrBeer = $(this).hasClass("users");
    const usersSearchLink = "/admin/users-filtered"
    const beersSearchLink = "/admin/beers-filtered"
    const closestForm = $(this).closest("form");
    const currentPageUrl = isUserOrBeer ? usersSearchLink : beersSearchLink;
    let additionalParameters = extractAdditionalParameters(closestForm);
    const url = currentPageUrl + additionalParameters;
    isUserOrBeer ? $('.user-results').load(url) : $('.beer-results').load(url);
}

function getSearchResultsHome(event) {
    debugger
    event.preventDefault();
    const homeSearchLink = "/filter"
    const closestForm = $(this).closest("form");
    let additionalParameters = extractAdditionalParameters(closestForm);
    const url = homeSearchLink + additionalParameters;
    $('.search-index').load(url);
    $("#top-beers-header").text("SEARCH RESULTS:");
}


function extractAdditionalParameters(closestForm) {
    let additionalParameters = "?"
    $.each(closestForm.serializeArray(), function (_, kv) {
        additionalParameters += kv.name + "=" + kv.value + "&";
    });
    return additionalParameters;
}

function getBreweries() {
    const breweriesTag = $(".brewery-dropdown");
    debugger
    const countryId = $(this).val();
    if (countryId !== "" && countryId !== -1) {
        debugger
        const url = "/api/countries/" + countryId + "/breweries";

        $.ajax({
            url: url,
            type: "GET",
            dataType: "JSON",
            data: {},
            success: function (data) {
                debugger
                const len = data.length;
                let selectMessage;
                breweriesTag.empty();
                len === 0 ? selectMessage = "No breweries for this country yet" : selectMessage = "Select brewery";
                breweriesTag.append("<option value='' selected>" + selectMessage + "</option>");
                for (let i = 0; i < len; i++) {
                    const id = data[i]['id'];
                    const name = data[i]['name'];
                    breweriesTag.append("<option value='" + id + "'>" + name + "</option>");
                }
                if (breweriesTag[0].hasAttribute("hidden") && len !== 0) {
                    debugger
                    breweriesTag[0].removeAttribute("hidden");
                }
            },
            error: function () {
                alert("Calculating policy is currently unavailable :(")
            }
        });
    } else {
        breweriesTag.empty();
        breweriesTag.val(" ");
        breweriesTag.append("<option value=' ' selected>" + "Any brewery..." + "</option>");
    }
}