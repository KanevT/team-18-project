package com.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int id;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "contest_phase_id")
    private Phase phase;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "finish_date")
    private Date finishDate;

    @Column(name = "from_hour")
    private Date phase2FromHour;

    @Column(name = "to_hour")
    private Date phase2ToHour;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private Type type;

    @ManyToOne
    @JoinColumn(name = "cover_photo")
    private Image coverPhoto;

    @Column(name = "assessed")
    private Boolean isAssessed;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contest_users",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> users = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "images_contest",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "img_id")
    )
    private Set<Image> images = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contest_jury",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> jury = new HashSet<>();

    public Contest() {
    }

    public Contest(int id,
                   String title,
                   Phase phase,
                   Date startDate,
                   Date finishDate,
                   Date phase2FromHour,
                   Date phase2ToHour,
                   Category category,
                   Type type,
                   Image coverPhoto,
                   Set<User> users,
                   Set<Image> images,
                   Set<User> jury) {
        this.id = id;
        this.title = title;
        this.phase = phase;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.phase2FromHour = phase2FromHour;
        this.phase2ToHour = phase2ToHour;
        this.category = category;
        this.type = type;
        this.coverPhoto = coverPhoto;
        this.users = users;
        this.images = images;
        this.jury = jury;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Phase getPhase() {
        return phase;
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> userSet) {
        this.users = userSet;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> imageList) {
        this.images = imageList;
    }

    public Date getPhase2FromHour() {
        return phase2FromHour;
    }

    public void setPhase2FromHour(Date phase2FromHour) {
        this.phase2FromHour = phase2FromHour;
    }

    public Date getPhase2ToHour() {
        return phase2ToHour;
    }

    public void setPhase2ToHour(Date phase2ToHour) {
        this.phase2ToHour = phase2ToHour;
    }

    public Set<User> getJury() {
        return jury;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setJury(Set<User> jurySet) {
        this.jury = jurySet;
    }

    public Image getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(Image coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public Boolean getAssessed() {
        return isAssessed;
    }

    public void setAssessed(Boolean assessed) {
        isAssessed = assessed;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Contest contest = (Contest) object;
        return id == contest.id &&
                Objects.equals(title, contest.title) &&
                Objects.equals(phase, contest.phase) &&
                Objects.equals(startDate, contest.startDate) &&
                Objects.equals(finishDate, contest.finishDate) &&
                Objects.equals(phase2FromHour, contest.phase2FromHour) &&
                Objects.equals(phase2ToHour, contest.phase2ToHour) &&
                Objects.equals(category, contest.category) &&
                Objects.equals(type, contest.type) &&
                Objects.equals(coverPhoto, contest.coverPhoto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, phase, startDate, finishDate, phase2FromHour, phase2ToHour, category, type, coverPhoto);
    }
}
