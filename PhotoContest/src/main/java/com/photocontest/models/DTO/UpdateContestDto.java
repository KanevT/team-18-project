package com.photocontest.models.DTO;

import com.photocontest.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateContestDto {

    private String title;

    @NotNull(message = Constants.CONTEST_DATE_NULL_MESSAGE)
    @Positive(message = Constants.CONTEST_DATE_POSITIVE_MESSAGE)
    private String startDate;

    @NotNull(message = Constants.CONTEST_DATE_NULL_MESSAGE)
    @Positive(message = Constants.CONTEST_DATE_POSITIVE_MESSAGE)
    private String finishDate;

    @NotNull(message = Constants.CATEGORY_ID_NULL_MESSAGE)
    @Positive(message = Constants.CATEGORY_ID_POSITIVE_MESSAGE)
    private int category;

    private String coverPhoto;

    private String fromHour;

    private String toHour;

    public UpdateContestDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getFromHour() {
        return fromHour;
    }

    public void setFromHour(String fromHour) {
        this.fromHour = fromHour;
    }

    public String getToHour() {
        return toHour;
    }

    public void setToHour(String toHour) {
        this.toHour = toHour;
    }
}
