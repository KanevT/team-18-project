package com.photocontest.models.DTO;

import com.photocontest.helpers.annotations.PasswordValueMatch;
import com.photocontest.helpers.annotations.ValidPassword;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@PasswordValueMatch.List({
        @PasswordValueMatch(
                field = "newPassword",
                fieldMatch = "confirmPassword",
                message = "Passwords do not match!"
        )
})
public class UpdatePasswordDto {

    private Integer id;

    @NotEmpty
    private String oldPassword;

    @ValidPassword
    private String newPassword;
    private String confirmPassword;
}
