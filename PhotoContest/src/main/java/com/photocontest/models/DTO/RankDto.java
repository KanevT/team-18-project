package com.photocontest.models.DTO;

import com.photocontest.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RankDto {

    @NotNull(message = Constants.RANG_NAME_NULL_MESSAGE)
    @Size(min = 2, max = 15, message = Constants.RANG_NAME_MESSAGE)
    private String name;

    public RankDto() {
    }

    public RankDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
