package com.photocontest.models.DTO;

import com.photocontest.utils.Constants;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class UserDto {

    @NotEmpty
    private String username;

    @Size(min = 8, message = Constants.PASSWORD_SYMBOLS_MESSAGE)
    private String password;

    @Size(min = 2, max = 20, message = Constants.USER_NAME_SYMBOLS_MESSAGE)
    private String firstName;

    @Size(min = 2, max = 20, message = Constants.USER_NAME_SYMBOLS_MESSAGE)
    private String lastName;

    @NotNull(message = Constants.EMAIL_CAN_NULL_MESSAGE)
    private String email;

    public UserDto(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
