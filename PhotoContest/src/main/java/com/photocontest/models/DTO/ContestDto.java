package com.photocontest.models.DTO;

import com.photocontest.models.User;
import com.photocontest.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

public class ContestDto {

    @NotNull(message = Constants.PHASE_ID_NULL_MESSAGE)
    private String title;

    @NotNull(message = Constants.PHASE_ID_NULL_MESSAGE)
    private int phase;

    @NotNull(message = Constants.CONTEST_DATE_NULL_MESSAGE)
    private String startDate;

    @NotNull(message = Constants.CONTEST_DATE_NULL_MESSAGE)
    private String finishDate;

    @NotNull(message = Constants.CATEGORY_ID_NULL_MESSAGE)
    @Positive(message = Constants.CATEGORY_ID_POSITIVE_MESSAGE)
    private int category;

    private String type;

    private String coverPhoto;

    private String fromHour;

    private String toHour;

    private String jurySet;

    private String userSet;

    public ContestDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPhase() {
        return phase;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getFromHour() {
        return fromHour;
    }

    public void setFromHour(String fromHour) {
        this.fromHour = fromHour;
    }

    public String getToHour() {
        return toHour;
    }

    public void setToHour(String toHour) {
        this.toHour = toHour;
    }

    public String getJurySet() {
        return jurySet;
    }

    public void setJurySet(String jurySet) {
        this.jurySet = jurySet;
    }

    public String getUserSet() {
        return userSet;
    }

    public void setUserSet(String userSet) {
        this.userSet = userSet;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
