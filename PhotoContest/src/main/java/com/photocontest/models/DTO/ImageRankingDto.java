package com.photocontest.models.DTO;

public class ImageRankingDto {

    private int image;

    private long points;

    public ImageRankingDto() {
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }
}
