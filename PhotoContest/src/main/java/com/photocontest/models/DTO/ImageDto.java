package com.photocontest.models.DTO;

import com.photocontest.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ImageDto {

    @NotNull(message = Constants.IMAGE_TITLE_NULL_MESSAGE)
    @Size(min = 2, max = 50, message = Constants.IMAGE_TITLE_MESSAGE)
    private String title;

    @NotNull(message = Constants.IMAGE_STORY_NULL_MESSAGE)
    @Size(min = 10, message = Constants.IMAGE_STORY_MESSAGE)
    private String story;

    @NotNull
    private String url;

    public ImageDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
