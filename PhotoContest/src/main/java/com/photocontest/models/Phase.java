package com.photocontest.models;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "contest_phases")
public class Phase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_phases_id")
    private int id;

    @Column(name = "phase_name")
    private String name;

    public Phase() {
    }

    public Phase(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Phase phase = (Phase) object;
        return id == phase.id &&
                name.equals(phase.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
