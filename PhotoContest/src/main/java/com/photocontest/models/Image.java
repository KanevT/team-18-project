package com.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "images")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "img_id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "story")
    private String story;

    @Column(name = "img_url")
    private String imageUrl;

    @Column(name = "winner_place")
    private int winnerPlace;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "images_reviews",
            joinColumns = @JoinColumn(name = "image_id"),
            inverseJoinColumns = @JoinColumn(name = "review_id")
    )
    private Set<Review> reviews = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_images",
            joinColumns = @JoinColumn(name = "image_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> users = new ArrayList<>();


    public Image() {
    }

    public Image(int id, String title, String story, String imageUrl, int isWinner, Set<Review> reviews, List<User> users) {
        this.id = id;
        this.title = title;
        this.story = story;
        this.imageUrl = imageUrl;
        this.winnerPlace = isWinner;
        this.reviews = reviews;
        this.users = users;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviewList) {
        this.reviews = reviewList;
    }

    private List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> user) {
        this.users = user;
    }

    public int getWinnerPlace() {
        return winnerPlace;
    }

    public void setWinnerPlace(int isWinner) {
        this.winnerPlace = isWinner;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Image image = (Image) object;
        return id == image.id &&
                winnerPlace == image.winnerPlace &&
                title.equals(image.title) &&
                story.equals(image.story) &&
                Objects.equals(imageUrl, image.imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, story, imageUrl, winnerPlace);
    }

    @JsonIgnore
    public User getImageUser() {
        List<User> users = getUsers();
        return users.get(0);
    }

    @JsonIgnore
    public int getImageReviewsPoints() {
        Set<Review> reviews = getReviews();
        int allPoints = 0;
        for (Review review : reviews) {
            allPoints += review.getPoints();
        }
        return allPoints;
    }
}
