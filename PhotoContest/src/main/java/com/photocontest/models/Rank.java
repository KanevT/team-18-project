package com.photocontest.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ranks")
public class Rank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rank_id")
    private int id;

    @Column(name = "rank_name")
    private String name;


    public Rank() {
    }

    public Rank(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Rank rank = (Rank) object;
        return id == rank.id &&
                name.equals(rank.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
