package com.photocontest.repositories.contracts;

import com.photocontest.models.Phase;

import java.util.List;

public interface PhaseRepository {
    List<Phase> getAll();

    Phase getById(int id);

    Phase getByName(String name);
}
