package com.photocontest.repositories.contracts;

import com.photocontest.models.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository {

    List<Category> getAll();

    Category getById(int id);

    Category getByName(String name);
}
