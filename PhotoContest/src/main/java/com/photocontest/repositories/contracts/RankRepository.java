package com.photocontest.repositories.contracts;


import com.photocontest.models.Rank;

import java.util.List;

public interface RankRepository {

    List<Rank> getAll();

    Rank getByName(String name);

    Rank getById(int id);

    Rank create(Rank role);
}
