package com.photocontest.repositories.contracts;

import com.photocontest.models.Comment;

import java.util.List;

public interface CommentRepository {

    List<Comment> getAll();

    Comment getById(int id);

    void create(Comment comment);
}
