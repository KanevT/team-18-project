package com.photocontest.repositories.contracts;

import com.photocontest.models.Contest;
import com.photocontest.models.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserRepository {

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    User getByFirsName(String firstName);

    User getByLastName(String lastName);

    User getByEmail(String email);

    void create(User user);

    void update(User user);

    void delete(int id);

    List<User> searchByMultiple(Optional<String> firstName,
                                Optional<String> lastName,
                                Optional<String> email,
                                Optional<String> username);

    List<User> searchByOneWord(Optional<String> oneWord);

    Long getCustomerCount();

    List<User> getAdminList(String type);
}
