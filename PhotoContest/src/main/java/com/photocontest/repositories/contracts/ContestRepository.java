package com.photocontest.repositories.contracts;

import com.photocontest.models.Contest;

import java.util.List;
import java.util.Optional;

public interface ContestRepository {

    List<Contest> getAll();

    Contest getById(int id);

    Contest getByTitle(String title);

    List<Contest> getByPhase(String phaseName);

    void create(Contest contest);

    void update(Contest contest);

    void delete(int id);

    List<Contest> filter(Optional<Integer> phaseId,
                         Optional<String> title);
}
