package com.photocontest.repositories;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Category;
import com.photocontest.models.Contest;
import com.photocontest.models.Image;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ContestRepositoryImpl implements ContestRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Contest> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Contest ", Contest.class).list();
        }
    }

    @Override
    public Contest getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Contest contest = session.get(Contest.class, id);

            if (contest == null) {
                throw new EntityNotFoundException("Contest", id);
            }
            return contest;
        }
    }

    @Override
    public Contest getByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where title = :name", Contest.class);
            query.setParameter("name", title);
            List<Contest> contestList = query.list();

            if (contestList.size() == 0) {
                throw new EntityNotFoundException("Contest", "title", title);
            }
            return contestList.get(0);
        }
    }

    @Override
    public List<Contest> getByPhase(String phaseName) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Contest  where phase.name like concat('%', :name, '%')", Contest.class)
                    .setParameter("name", phaseName)
                    .getResultList();
        }
    }

    @Override
    public void create(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.save(contest);
        }
    }

    @Override
    public void update(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(contest);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Contest contest = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(contest);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Contest> filter(Optional<Integer> phaseName,
                                Optional<String> title) {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Contest c where c.phase.id = :id  and" +
                    " c.title like concat('%', :title, '%')", Contest.class);

            query.setParameter("id", phaseName.orElse(0));
            query.setParameter("title", title.orElse(""));

            return query.list();
        }
    }
}
