package com.photocontest.repositories;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Type;
import com.photocontest.repositories.contracts.TypeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TypeRepositoryImpl implements TypeRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TypeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Type> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Type ", Type.class)
                    .getResultList();
        }
    }

    @Override
    public Type getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Type type = session.get(Type.class, id);
            if (type == null) {
                throw new EntityNotFoundException("Type", id);
            }
            return type;
        }
    }

    @Override
    public Type getByName(String name) {
        try (Session session = sessionFactory.openSession()) {

            Query<Type> query = session.createQuery("from Type where name =:name", Type.class);
            query.setParameter("name", name);

            List<Type> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Type", "name", name);
            }
            return result.get(0);
        }
    }
}
