package com.photocontest.repositories;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Category;
import com.photocontest.models.Rank;
import com.photocontest.repositories.contracts.CategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Category ", Category.class).list();
        }
    }

    @Override
    public Category getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Category category = session.get(Category.class, id);

            if (category == null) {
                throw new EntityNotFoundException("Category", id);
            }
            return category;
        }
    }

    @Override
    public Category getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Category  where name = :name", Category.class)
                    .setParameter("name", name)
                    .uniqueResult();
        }
    }
}
