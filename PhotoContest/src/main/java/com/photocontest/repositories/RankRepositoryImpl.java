package com.photocontest.repositories;

import com.photocontest.models.Rank;
import com.photocontest.repositories.contracts.RankRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RankRepositoryImpl implements RankRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RankRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Rank> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Rank", Rank.class)
                    .list();
        }
    }

    @Override
    public Rank getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Rank  where name = :name", Rank.class)
                    .setParameter("name", name)
                    .uniqueResult();
        }
    }

    @Override
    public Rank getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Rank  where id = :id", Rank.class)
                    .setParameter("id", id)
                    .uniqueResult();
        }
    }

    @Override
    public Rank create(Rank rank) {
        try (Session session = sessionFactory.openSession()) {
            session.save(rank);
        }
        return rank;
    }
}