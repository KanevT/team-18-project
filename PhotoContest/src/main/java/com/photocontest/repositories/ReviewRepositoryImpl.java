package com.photocontest.repositories;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Contest;
import com.photocontest.models.Review;
import com.photocontest.repositories.contracts.ReviewRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReviewRepositoryImpl implements ReviewRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ReviewRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Review> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Review ", Review.class).list();
        }
    }

    @Override
    public Review getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Review review = session.get(Review.class, id);

            if (review == null) {
                throw new EntityNotFoundException("Review", id);
            }
            return review;
        }
    }

    @Override
    public void create(Review review) {
        try (Session session = sessionFactory.openSession()) {
            session.save(review);
        }
    }

    @Override
    public void update(Review review) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(review);
            session.getTransaction().commit();
        }
    }
}
