package com.photocontest.repositories;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Image;
import com.photocontest.repositories.contracts.ImageRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ImageRepositoryImpl implements ImageRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ImageRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Image> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Image", Image.class)
                    .getResultList();
        }
    }

    @Override
    public Image getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Image image = session.get(Image.class, id);
            if (image == null) {
                throw new EntityNotFoundException("Image", id);
            }
            return image;
        }
    }

    @Override
    public void create(Image image) {
        try (Session session = sessionFactory.openSession()) {
            session.save(image);
        }
    }


    @Override
    public List<Image> searchByTitleOrStory(Optional<String> title,
                                            Optional<String> story) {

        try (Session session = sessionFactory.openSession()) {
            Query<Image> query = session.createQuery("from Image where title like concat('%', :title, '%')  and" +
                    " story like concat('%', :story, '%') ", Image.class);

            query.setParameter("title", title.orElse(""));
            query.setParameter("story", story.orElse(""));

            return query.list();
        }
    }


    @Override
    public void update(Image image) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(image);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Image> getWinnerImages() {
        try (Session session = sessionFactory.openSession()) {
            Query<Image> query = session.createQuery("from Image where winnerPlace > 0");
            return query.list();
        }
    }
}