package com.photocontest.mappers;

import com.photocontest.models.DTO.PhaseDto;
import com.photocontest.models.Phase;
import com.photocontest.repositories.contracts.PhaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PhaseModelMapper {

    private final PhaseRepository phaseRepository;

    @Autowired
    public PhaseModelMapper(PhaseRepository phaseRepository) {
        this.phaseRepository = phaseRepository;
    }

    public Phase fromDto(PhaseDto phaseDto) {
        Phase phase = new Phase();
        dtoToObject(phaseDto, phase);
        return phase;
    }

    private void dtoToObject(PhaseDto phaseDto, Phase phase) {
        phase.setName(phaseDto.getName());
    }

    public PhaseDto toDto(Phase phase) {
        PhaseDto phaseDto = new PhaseDto();
        phaseDto.setName(phase.getName());
        return phaseDto;
    }
}
