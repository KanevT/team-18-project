package com.photocontest.mappers;

import com.photocontest.models.Contest;
import com.photocontest.models.DTO.UpdateContestDto;
import com.photocontest.models.Image;
import com.photocontest.repositories.contracts.CategoryRepository;
import com.photocontest.repositories.contracts.ImageRepository;
import com.photocontest.repositories.contracts.PhaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class UpdateContestModelMapper {

    private final CategoryRepository categoryRepository;
    private final ImageRepository imageRepository;
    private final PhaseRepository phaseRepository;

    @Autowired
    public UpdateContestModelMapper(CategoryRepository categoryRepository,
                                    ImageRepository imageRepository, PhaseRepository phaseRepository) {
        this.categoryRepository = categoryRepository;
        this.imageRepository = imageRepository;
        this.phaseRepository = phaseRepository;
    }

    public Contest dtoToObjectUpdate(UpdateContestDto updateContestDto, Contest contest) throws ParseException {
        contest.setTitle(updateContestDto.getTitle());
        contest.setCategory(categoryRepository.getById(updateContestDto.getCategory()));
        contest.setStartDate(convertStartDate(updateContestDto));
        contest.setFinishDate(convertFinishtDate(updateContestDto));
        contest.setPhase2FromHour(convertPhase2FromHour(updateContestDto));
        contest.setPhase2ToHour(convertPhase2ToHour(updateContestDto));
        contest.setPhase(phaseRepository.getById(2));
        Image image = contest.getCoverPhoto();
        Image imageToUpdate = imageRepository.getById(image.getId());
        imageToUpdate.setImageUrl(updateContestDto.getCoverPhoto());
        imageRepository.update(imageToUpdate);
        return contest;
    }

    private Date convertStartDate(UpdateContestDto updateContestDto) throws ParseException {
        String startDate = updateContestDto.getStartDate();
        return new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
    }

    private Date convertFinishtDate(UpdateContestDto updateContestDto) throws ParseException {
        String finishDate = updateContestDto.getFinishDate();
        return new SimpleDateFormat("yyyy-MM-dd").parse(finishDate);
    }

    private Date convertPhase2FromHour(UpdateContestDto updateContestDto) throws ParseException {
        String finishDate = updateContestDto.getFinishDate();
        String source = updateContestDto.getFromHour();
        String[] tokens = source.split(":");
        String date = finishDate + " " + tokens[0] + ":" + tokens[1];
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date);
    }

    private Date convertPhase2ToHour(UpdateContestDto updateContestDto) throws ParseException {
        String finishDate = updateContestDto.getFinishDate();
        String source = updateContestDto.getToHour();
        String[] tokens = source.split(":");
        String date = finishDate + " " + tokens[0] + ":" + tokens[1];
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date);
    }
}
