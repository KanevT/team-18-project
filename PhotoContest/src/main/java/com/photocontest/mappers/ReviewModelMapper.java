package com.photocontest.mappers;

import com.photocontest.models.Comment;
import com.photocontest.models.DTO.RateDto;
import com.photocontest.models.Review;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.CommentRepository;
import com.photocontest.repositories.contracts.ReviewRepository;
import com.photocontest.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ReviewModelMapper {

    private final CommentRepository commentRepository;
    private final ReviewRepository reviewRepository;

    @Autowired
    public ReviewModelMapper(CommentRepository commentRepository, ReviewRepository reviewRepository) {
        this.commentRepository = commentRepository;
        this.reviewRepository = reviewRepository;
    }

    public Review fromDto(RateDto rateDto, User user, int isComplain) {
        Review review = new Review();
        Comment comment = new Comment();
        dtoToObject(rateDto, review, comment, user, isComplain);
        return review;
    }

    private void dtoToObject(RateDto rateDto, Review review, Comment comment, User user, int isComplain) {

        if (isComplain == 1) {
            comment.setValue(Constants.THE_CATEGORY_IS_WRONG);
            review.setPoints(0);
        } else {
            comment.setValue(rateDto.getComment().trim());
            review.setPoints(rateDto.getPoints());
        }

        review.setUser(user);
        review.setComment(comment);

        commentRepository.create(comment);
        reviewRepository.create(review);
    }
}
