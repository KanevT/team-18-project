package com.photocontest.mappers;

import com.photocontest.models.DTO.UserDto;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.RankRepository;
import com.photocontest.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserModelMapper {

    private final UserRepository userRepository;
    private final RankRepository rankRepository;

    @Autowired
    public UserModelMapper(UserRepository userRepository, RankRepository rankRepository) {
        this.userRepository = userRepository;
        this.rankRepository = rankRepository;
    }

    public User fromDto(UserDto userDto) {
        User customer = new User();
        dtoToObject(userDto, customer);
        return customer;
    }


    public User fromDto(UserDto userDto, int id) {
        User customer = userRepository.getById(id);
        dtoToObject(userDto, customer);
        return customer;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        return userDto;
    }

    public User inversion(User inputUser, User updateUser) {
        inputUser.setUsername(updateUser.getUsername());
        inputUser.setPassword(updateUser.getPassword());
        inputUser.setFirstName(updateUser.getFirstName());
        inputUser.setLastName(updateUser.getLastName());
        inputUser.setEmail(updateUser.getEmail());
        inputUser.setPicture(updateUser.getPicture());

        return inputUser;
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setRanks(Set.of(rankRepository.getById(2)));
        user.setScores(0);
        user.setPicture("https://datasciencelab.unimi.it/wp-content/uploads/2017/06/user.png");
    }
}
