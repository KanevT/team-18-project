package com.photocontest.mappers;

import com.photocontest.models.Contest;
import com.photocontest.models.DTO.ContestDto;
import com.photocontest.models.Image;
import com.photocontest.repositories.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ContestModelMapper {

    private final ContestRepository contestRepository;
    private final CategoryRepository categoryRepository;
    private final PhaseRepository phaseRepository;
    private final ImageRepository imageRepository;
    private final TypeRepository typeRepository;

    @Autowired
    public ContestModelMapper(ContestRepository contestRepository,
                              CategoryRepository categoryRepository,
                              PhaseRepository phaseRepository,
                              ImageRepository imageRepository,
                              TypeRepository typeRepository) {
        this.contestRepository = contestRepository;
        this.categoryRepository = categoryRepository;
        this.phaseRepository = phaseRepository;
        this.imageRepository = imageRepository;
        this.typeRepository = typeRepository;
    }


    public Contest fromDto(ContestDto contestDto, String type) throws ParseException {
        Contest contest = new Contest();
        dtoToObject(contestDto, contest, type);
        return contest;
    }

    public Contest fromDto(ContestDto contestDto, int id, String type) throws ParseException {
        Contest contest = contestRepository.getById(id);
        dtoToObject(contestDto, contest, type);
        return contest;
    }

    public ContestDto toDto(Contest contest) {
        ContestDto contestDto = new ContestDto();
        contestDto.setTitle(contest.getTitle());
        contestDto.setPhase(contest.getPhase().getId());
        contestDto.setType(contest.getType().getName());
        contestDto.setCategory(contest.getCategory().getId());
        contestDto.setStartDate(contest.getStartDate().toString());
        contestDto.setFinishDate(contest.getFinishDate().toString());
        contestDto.setFromHour(contest.getPhase2FromHour().toString());
        contestDto.setToHour(contest.getPhase2ToHour().toString());
        contestDto.setCoverPhoto(contest.getCoverPhoto().getImageUrl());

        return contestDto;
    }

    private void dtoToObject(ContestDto contestDto, Contest contest, String type) throws ParseException {
        contest.setTitle(contestDto.getTitle());
        contest.setPhase(phaseRepository.getById(2));
        contest.setType(typeRepository.getByName(type));
        contest.setCategory(categoryRepository.getById(contestDto.getCategory()));
        contest.setStartDate(convertStartDate(contestDto));
        contest.setFinishDate(convertFinishDate(contestDto));
        contest.setPhase2FromHour(convertPhase2FromHour(contestDto));
        contest.setPhase2ToHour(convertPhase2ToHour(contestDto));
        contest.setAssessed(false);
        contest.setCoverPhoto(createImage(contestDto));
    }

    private Date convertStartDate(ContestDto contestDto) throws ParseException {
        String startDate = contestDto.getStartDate();
        return new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
    }

    private Date convertFinishDate(ContestDto contestDto) throws ParseException {
        String finishDate = contestDto.getFinishDate();
        return new SimpleDateFormat("yyyy-MM-dd").parse(finishDate);
    }

    private Date convertPhase2FromHour(ContestDto contestDto) throws ParseException {
        String finishDate = contestDto.getFinishDate();
        String source = contestDto.getFromHour();
        String[] tokens = source.split(":");
        String date = finishDate + " " + tokens[0] + ":" + tokens[1];
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date);
    }

    private Date convertPhase2ToHour(ContestDto contestDto) throws ParseException {
        String finishDate = contestDto.getFinishDate();
        String source = contestDto.getToHour();
        String[] tokens = source.split(":");
        String date = finishDate + " " + tokens[0] + ":" + tokens[1];
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date);
    }

    private Image createImage(ContestDto contestDto) {
        if (contestDto.getCoverPhoto() == null) {
            contestDto.setCoverPhoto("https://image.freepik.com/free-vector/coming-soon-background-with-spotlight_23-2147501119.jpg?1");
        }
        Image image = new Image();
        image.setId(1);
        image.setImageUrl(contestDto.getCoverPhoto());
        image.setStory(contestDto.getTitle());
        image.setTitle(contestDto.getTitle());
        imageRepository.create(image);
        return image;
    }
}

