package com.photocontest.mappers;

import com.photocontest.models.Contest;
import com.photocontest.models.DTO.ContestDto;
import com.photocontest.models.DTO.ImageDto;
import com.photocontest.models.Image;
import com.photocontest.models.Review;
import com.photocontest.repositories.contracts.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ImageModelMapper {

    private final ImageRepository imageRepository;

    @Autowired
    public ImageModelMapper(ImageRepository imageRepository) {

        this.imageRepository = imageRepository;
    }

    public Image fromDto(ImageDto imageDto) {
        Image image = new Image();
        dtoToObject(imageDto, image);
        return image;
    }

    public Image fromDto(ImageDto imageDto, int id) {
        Image image = imageRepository.getById(id);
        dtoToObject(imageDto, image);
        return image;
    }

    private void dtoToObject(ImageDto imageDto, Image image) {
        image.setTitle(imageDto.getTitle());
        image.setStory(imageDto.getStory().trim());

        if (imageDto.getUrl().isEmpty()) {
            image.setImageUrl("https://1080motion.com/wp-content/uploads/2018/06/NoImageFound.jpg.png");
        }

        image.setImageUrl(imageDto.getUrl());
    }

    public void setCoverPhotoLocal(Contest contest, String imageUrl) {
        Image imageToUpdate = contest.getCoverPhoto();
        imageToUpdate.setImageUrl(imageUrl);
        imageRepository.update(imageToUpdate);
        contest.setCoverPhoto(imageToUpdate);
    }
}
