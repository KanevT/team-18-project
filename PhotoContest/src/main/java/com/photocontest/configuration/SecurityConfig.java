package com.photocontest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers("/assets/**", "/css/**", "/fonts/**", "/img/**", "/js/**", "/api/**").permitAll()
//                .antMatchers("/login/**", "/register/**", "/logout/**").permitAll()
//                .antMatchers("/", "/filter","/categories/**", "/contests/**", "/users/**", "/users/{id}/**", "/error").permitAll()
//                .antMatchers("/contests/{id}/**", "/contests/create/", "/users/personal/**").hasRole("USER")
//                .antMatchers("/admin").hasRole("ADMIN")
//                .anyRequest()
//                .authenticated()
//                .and()
//                .formLogin()
//                .loginPage("/login")
//                .loginProcessingUrl("/authenticate")
//                .successHandler(new SimpleUrlAuthenticationSuccessHandler())
//                .permitAll()
//                .and()
//                .logout()
//                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//                .deleteCookies("JSESSIONID")
//                .permitAll()
//                .and()
//                .rememberMe().key("uniqueAndSecret")
//                .and()
//                .csrf().disable();
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().permitAll()
                .and()
                .csrf().disable();
    }
}

