package com.photocontest.exceptions;

public class RuleDeleteException  extends RuntimeException{

    public RuleDeleteException() {
    }

    public RuleDeleteException(String message) {
        super(message);
    }

}
