package com.photocontest.exceptions;

public class InvalidDtoDataException extends RuntimeException {

    public InvalidDtoDataException() {
    }

    public InvalidDtoDataException(String message) {
        super(message);
    }
}
