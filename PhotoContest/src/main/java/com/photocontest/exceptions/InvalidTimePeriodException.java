package com.photocontest.exceptions;

public class InvalidTimePeriodException extends RuntimeException {

    public InvalidTimePeriodException(String desc, int timeMin, int timeMax, String length) {
        super(String.format("%s must continue between %d and %d %s", desc, timeMin, timeMax, length));
    }

    public InvalidTimePeriodException() {
    }

    public InvalidTimePeriodException(String message) {
        super(message);
    }
}
