package com.photocontest.exceptions;

public class ЕvaluationCheckPictureException extends RuntimeException {

    public ЕvaluationCheckPictureException() {
    }

    public ЕvaluationCheckPictureException(String message) {
        super(message);
    }
}
