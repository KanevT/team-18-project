package com.photocontest.exceptions;


import com.photocontest.utils.Constants;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format(Constants.DUPLICATE_ENTITY_ERROR, type, attribute, value));
    }

    public DuplicateEntityException(String message) {
        super(message);
    }
}
