package com.photocontest.services;

import com.photocontest.exceptions.*;
import com.photocontest.models.Contest;
import com.photocontest.models.DTO.ContestDto;
import com.photocontest.models.Image;
import com.photocontest.models.Review;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.ContestRepository;
import com.photocontest.repositories.contracts.UserRepository;
import com.photocontest.services.contracts.ContestService;
import com.photocontest.utils.PaginationHelper;
import com.photocontest.utils.VerifyHelper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.photocontest.utils.Constants.*;

@Service
public class ContestServiceImpl implements ContestService {


    private final ContestRepository contestRepository;
    private final UserRepository userRepository;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository,
                              UserRepository userRepository) {
        this.contestRepository = contestRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Contest> getAll(User user) {

        VerifyHelper.verifyIfAdmin(user);

        return contestRepository.getAll();
    }

    @Override
    public Contest getById(int id, User user) {

        VerifyHelper.verifyIfAnonymous(user);

        return contestRepository.getById(id);
    }

    @Override
    public List<Contest> getByPhase(Optional<String> phaseName) {

        List<Contest> result = null;

        if (phaseName.isPresent()) {
            result = contestRepository.getByPhase(phaseName.get());
            if (result == null) {
                throw new EntityNotFoundException(PHASE, NAME, phaseName.get());
            }
        }
        return result;
    }

    @Override
    public List<Contest> getByPhaseOne(String phaseName, User user) {

        VerifyHelper.verifyIfAdmin(user);

        List<Contest> result;
        result = contestRepository.getByPhase(phaseName);

        if (result == null) {
            throw new EntityNotFoundException(PHASE, NAME, phaseName);
        }

        return result;
    }

    @Override
    public List<Contest> filter(Optional<Integer> phaseName, Optional<String> title, User user) {

        VerifyHelper.verifyIfAdmin(user);

        return contestRepository.filter(phaseName, title);
    }

    @Override
    public void create(Contest contest, User user) {
        VerifyHelper.verifyIfAdmin(user);

        checkContestTimeLimit(contest);

        boolean duplicateExists = true;

        try {
            contestRepository.getByTitle(contest.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(CONTEST, TITLE, contest.getTitle());
        }

        contestRepository.create(contest);
    }

    @Override
    public void update(Contest contest, User user) {
        VerifyHelper.verifyIfAdmin(user);

        contestRepository.update(contest);

    }

    @Override
    public void delete(int id, User user) {
        VerifyHelper.verifyIfAdmin(user);

        Contest contestToDelete = contestRepository.getById(id);

        List<Contest> contests = contestRepository.getAll();

        for (Contest contest : contests) {
            if (contest.getImages().size() > 0) {
                throw new RuleDeleteException(
                        String.format(CONTEST_WITH_ID_D_CONTAINS_IMAGES_ERROR_MESSAGE, id));
            }
        }
        contestRepository.delete(id);
    }

    @Override
    public User addUserToContest(User user, Contest contest) {

        Set<User> newList = contest.getUsers();

        newList.add(user);
        contest.setUsers(newList);

        int scores = user.getScores() + POINT_FOR_JOIN_OPEN_CONTEST;
        user.setScores(scores);

        userRepository.update(user);
        contestRepository.update(contest);

        return user;
    }

    @Override
    public User addJuryToContest(User user, Contest contest) {

        Set<User> newList = contest.getJury();

        newList.add(user);
        contest.setJury(newList);

        contestRepository.update(contest);

        return user;
    }

    @Override
    public Set<User> getContestUserSet(User user, Contest contest) {
        VerifyHelper.verifyIfAdmin(user);

        return contest.getUsers();
    }

    @Override
    public Set<User> getContestJurySet(User user, Contest contest) {
        VerifyHelper.verifyIfAdmin(user);

        return contest.getJury();
    }

    @Override
    public void addJury(ContestDto contestDto, Contest contest) {

        Set<User> newList = contest.getJury();

        List<User> adminList = userRepository.getAdminList(ADMIN_RANK_USER);


        if (!(contestDto.getJurySet() == null)) {

            String[] jury = contestDto.getJurySet().split(",");

            for (int i = 0; i < jury.length; i++) {
                int userId = Integer.parseInt(jury[i]);
                User userToAdd = userRepository.getById(userId);
                newList.add(userToAdd);
            }
        }

        newList.addAll(adminList);
    }

    @Override
    public void checkIfCanBeJury(User user) {
        if (!user.isPhotoDictator() && !user.isAdmin()) {
            throw new InvalidDtoDataException(USER_CANNOT_BE_ADDED_MESSAGE);
        }
    }

    @Override
    public void addParticipants(ContestDto contestDto, Contest contest) {

        String[] jury = contestDto.getJurySet().split(",");
        String[] participants = contestDto.getUserSet().split(",");

        Set<User> juryList = contest.getJury();
        Set<User> participantsList = contest.getUsers();

        List<User> adminList = userRepository.getAdminList(ADMIN_RANK_USER);

        for (int i = 0; i < jury.length; i++) {
            int userId = Integer.parseInt(jury[i]);
            User userToAdd = userRepository.getById(userId);
            juryList.add(userToAdd);
        }

        for (int i = 0; i < participants.length; i++) {
            int userId = Integer.parseInt(participants[i]);
            User userToAdd = userRepository.getById(userId);
            int scores = userToAdd.getScores() + POINT_FOR_JOIN_INVITATIONAL_CONTEST;
            userToAdd.setScores(scores);
            userRepository.update(userToAdd);
            participantsList.add(userToAdd);
        }
        juryList.addAll(adminList);
        contest.setJury(juryList);
        contest.setUsers(participantsList);

        contestRepository.update(contest);
    }

    @Override
    public void checkIfJuryUserIsSelectedToParticipants(Contest contest) {

        Set<User> participantsList = contest.getUsers();

        participantsList.removeIf(user -> user.isAdmin() || user.isPhotoDictator());

        contestRepository.update(contest);
    }

    @Override
    public Image addImageToContest(Contest contest, Image image) {

        Set<Image> newList = contest.getImages();

        newList.add(image);
        contest.setImages(newList);
        contestRepository.update(contest);

        return image;
    }

    @Override
    public void checkIfUserIsRateImage(User currentUser, Image image) {
        Set<Review> filtered = image.getReviews().stream()
                .filter(mc -> mc.getUser().equals(currentUser))
                .collect(Collectors.toSet());

        if (filtered.size() > 0) {
            throw new ЕvaluationCheckPictureException(ALREADY_RATE_PHOTO);
        }
    }

    @Override
    public boolean checkIfUserAlreadyUploadPhoto(Contest contest, User currentUser) {

        boolean isUploadPhoto = false;

        for (Image image : contest.getImages()) {
            for (User user : contest.getUsers()) {
                if (image.getImageUser().equals(currentUser) && image.getImageUser().equals(user)) {
                    isUploadPhoto = true;
                }
            }
        }
        return isUploadPhoto;
    }

    private void checkContestTimeLimit(Contest contest) {

        // For PHASE 1!
        Date finishDateMinusOneMonth = new DateTime(contest.getFinishDate()).minusMonths(1).toDate();

        // For PHASE 2!
        Date finishDateMinusHours = new DateTime(contest.getPhase2ToHour()).minusHours(24).toDate();

        if (contest.getFinishDate().compareTo(contest.getStartDate()) < 0) {
            throw new InvalidTimePeriodException(THE_START_OF_PHASE_ONE_ERROR_MESSAGE);
        }
        if (finishDateMinusOneMonth.compareTo(contest.getStartDate()) > 0) {
            throw new InvalidTimePeriodException(CONTEST_PHASE_1, 1, 30, DAYS);
        }
        if (contest.getPhase2ToHour().compareTo(contest.getPhase2FromHour()) < 0) {
            throw new InvalidTimePeriodException(THE_START_OF_PHASE_TWO_ERROR_MESSAGE);
        }
        if (finishDateMinusHours.compareTo(contest.getPhase2FromHour()) > 0) {
            throw new InvalidTimePeriodException(PHASE_2, 1, 24, HOURS);
        }
    }

    @Override
    public Map<Integer, User> getImageReviewsUsers(Contest contest, User user) {
        Map<Integer, User> userAlreadyRatePhoto = new HashMap<>();

        for (Image image : contest.getImages()) {
            Set<Review> reviews = image.getReviews();
            for (Review review : reviews) {
                if (review.getUser().equals(user)) {
                    userAlreadyRatePhoto.put(image.getId(), user);
                }
            }
        }
        return userAlreadyRatePhoto;
    }
}