package com.photocontest.services;

import com.photocontest.models.Phase;
import com.photocontest.repositories.contracts.PhaseRepository;
import com.photocontest.services.contracts.PhaseService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhaseServiceImpl implements PhaseService {

    private final PhaseRepository phaseRepository;

    public PhaseServiceImpl(PhaseRepository phaseRepository) {
        this.phaseRepository = phaseRepository;
    }


    @Override
    public List<Phase> getAll() {
        return phaseRepository.getAll();
    }

    @Override
    public Phase getById(int id) {
        return phaseRepository.getById(id);
    }

    @Override
    public Phase getByName(String name) {
        return phaseRepository.getByName(name);
    }

}
