package com.photocontest.services;

import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Image;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.ImageRepository;
import com.photocontest.services.contracts.ImageService;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class ImageServiceImpl implements ImageService {

    private final ImageRepository imageRepository;

    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }


    @Override
    public List<Image> getAll() {
        return imageRepository.getAll();
    }

    @Override
    public Image getById(int id) {
        return imageRepository.getById(id);
    }


    @Override
    public void create(Image image) {
        imageRepository.create(image);
    }


    @Override
    public List<Image> searchByMultiply(Optional<String> title,
                                        Optional<String> story) {
        return imageRepository.searchByTitleOrStory(title, story);
    }

    @Override
    public List<Image> getWinnerImages() {
        return imageRepository.getWinnerImages();
    }


    @Override
    public String uploadImageToImgurAPI(String image) throws IOException, JSONException {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();

        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("image", image)
                .build();

        Request request = new Request.Builder()
                .url("https://api.imgur.com/3/image")
                .method("POST", body)
                .addHeader("Authorization", "Client-ID 28b16e3d6fc8970")
                .build();

        Response response = client.newCall(request).execute();

        JSONObject jsonObject = new JSONObject(response.body().string());
        JSONObject jsonObjectDate = jsonObject.getJSONObject("data");
        return jsonObjectDate.getString("link");
    }
}
