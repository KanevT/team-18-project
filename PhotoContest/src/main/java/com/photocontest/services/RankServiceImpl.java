package com.photocontest.services;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Rank;
import com.photocontest.repositories.contracts.RankRepository;
import com.photocontest.services.contracts.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RankServiceImpl implements RankService {

    private final RankRepository rankRepository;

    @Autowired
    public RankServiceImpl(RankRepository rankRepository) {
        this.rankRepository = rankRepository;
    }

    @Override
    public List<Rank> getAll() {
        return rankRepository.getAll();
    }

    @Override
    public Rank getById(int id) {
        Rank result = rankRepository.getById(id);
        if (result == null) {
            throw new EntityNotFoundException("Rank", id);
        }
        return result;
    }

    @Override
    public Rank getByName(Optional<String> name) {

        Rank result = null;

        if (name.isPresent()) {
            result = rankRepository.getByName(name.get());
            if (result == null) {
                throw new EntityNotFoundException("Rank", "name", name.get());
            }
        }
        return result;
    }
}
