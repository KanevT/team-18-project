package com.photocontest.services.contracts;

import com.photocontest.models.Contest;
import com.photocontest.models.DTO.UpdatePasswordDto;
import com.photocontest.models.Image;
import com.photocontest.models.Rank;
import com.photocontest.models.User;
import org.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserService {

    User getById(int id, User user);

    void create(User users);

    void update(User users, User userToUpdate);

    void delete(int id, User users);

    List<User> searchByMultiple(Optional<String> firstName,
                                Optional<String> lastName,
                                Optional<String> email,
                                Optional<String> username,
                                User user);

    List<User> getAll(User user);

    User getByUsername(String username);

    User getByFirstName(String firstName);

    User getByLastName(String lastName);

    User getByEmail(String email);

    List<User> searchByOneWord(Optional<String> oneWord,
                               User user);

    Long getCustomerCount();

    Rank addRank(User user, Rank rank);

    Set<Contest> getUserContests(int userId, User user);

    void passwordUpdate(UpdatePasswordDto updateData, User loggedUser);

    Page<User> findPaginated(Pageable pageable, List<User> allUsers);

    List<Contest> getUserActiveContests(User currentUser);

    List<Contest> getUserFinishedContests(User currentUser);

    Image addImageToUser(User user, Image image);
}
