package com.photocontest.services.contracts;

import com.photocontest.models.Contest;
import com.photocontest.models.DTO.ContestDto;
import com.photocontest.models.Image;
import com.photocontest.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public interface ContestService {

    List<Contest> getAll(User user);

    Contest getById(int id, User user);

    List<Contest> getByPhase(Optional<String> phaseName);

    List<Contest> filter(Optional<Integer> phaseId, Optional<String> title, User user);

    List<Contest> getByPhaseOne(String phaseName, User user);

    void create(Contest contest, User user);

    void update(Contest contest, User user);

    void delete(int id, User user);

    User addUserToContest(User user, Contest contest);

    User addJuryToContest(User user, Contest contest);

    Set<User> getContestUserSet(User user, Contest contest);

    Set<User> getContestJurySet(User user, Contest contest);

    void addJury(ContestDto contestDto, Contest newContest);

    void addParticipants(ContestDto contestDto, Contest newContest);

    void checkIfJuryUserIsSelectedToParticipants(Contest contest);

    Image addImageToContest(Contest contest, Image image);

    void checkIfUserIsRateImage(User currentUser, Image image);

    boolean checkIfUserAlreadyUploadPhoto(Contest contest, User currentUser);

    void checkIfCanBeJury(User user);

    Map<Integer, User> getImageReviewsUsers(Contest contest, User user);
}
