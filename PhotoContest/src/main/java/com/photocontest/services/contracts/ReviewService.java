package com.photocontest.services.contracts;

import com.photocontest.models.Image;
import com.photocontest.models.Review;
import com.photocontest.models.User;

import java.util.List;

public interface ReviewService {

    List<Review> getAll(Review review);

    Review getById(int id, User user);

    void create(Review review, User user);

    void update(Review review, User user);

    void addReviewToImage(Review review, Image image);
}
