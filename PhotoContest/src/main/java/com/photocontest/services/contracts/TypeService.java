package com.photocontest.services.contracts;

import com.photocontest.models.Type;

import java.util.List;

public interface TypeService {

    List<Type> getAll();

    Type getById(int id);

    Type getByName(String name);
}
