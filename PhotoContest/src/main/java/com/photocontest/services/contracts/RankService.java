package com.photocontest.services.contracts;

import com.photocontest.models.Rank;

import java.util.List;
import java.util.Optional;

public interface RankService {

    List<Rank> getAll();

    Rank getById(int id);

    Rank getByName(Optional<String> name);
}
