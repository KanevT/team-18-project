package com.photocontest.services.contracts;

import com.photocontest.models.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

    List<Category> getAll();

    Category getById(int id);

    Category getByName(Optional<String> name);
}
