package com.photocontest.services;

import com.photocontest.models.Image;
import com.photocontest.models.Review;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.ImageRepository;
import com.photocontest.repositories.contracts.ReviewRepository;
import com.photocontest.services.contracts.ReviewService;
import com.photocontest.utils.VerifyHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;
    private final ImageRepository imageRepository;

    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository, ImageRepository imageRepository) {
        this.reviewRepository = reviewRepository;
        this.imageRepository = imageRepository;
    }

    @Override
    public List<Review> getAll(Review review) {

        return reviewRepository.getAll();
    }

    @Override
    public Review getById(int id, User user) {
        VerifyHelper.verifyIfAnonymous(user);

        return reviewRepository.getById(id);
    }

    @Override
    public void create(Review review, User user) {
        VerifyHelper.verifyIfAdmin(user);

        reviewRepository.create(review);
    }

    @Override
    public void update(Review review, User user) {
        VerifyHelper.verifyIfAdmin(user);

        reviewRepository.update(review);
    }

    @Override
    public void addReviewToImage(Review review, Image image) {
        Set<Review> newList = image.getReviews();

        newList.add(review);
        image.setReviews(newList);
        imageRepository.update(image);
    }
}
