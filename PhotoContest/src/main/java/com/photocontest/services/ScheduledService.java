package com.photocontest.services;

import com.photocontest.models.Contest;
import com.photocontest.models.DTO.ImageRankingDto;
import com.photocontest.models.Image;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.photocontest.utils.Constants.*;

@Component
@EnableScheduling
public class ScheduledService {

    public static final int AWARD_DOUBLE_POINTS_FOR_FIRST_POSITION = 25;

    private final ContestRepository contestRepository;
    private final UserRepository userRepository;
    private final ImageRepository imageRepository;
    private final PhaseRepository phaseRepository;
    private final RankRepository rankRepository;

    @Autowired
    public ScheduledService(ContestRepository contestRepository,
                            UserRepository userRepository,
                            ImageRepository imageRepository,
                            PhaseRepository phaseRepository, RankRepository rankRepository) {
        this.contestRepository = contestRepository;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.phaseRepository = phaseRepository;
        this.rankRepository = rankRepository;
    }

    @Scheduled(fixedDelay = 60000)
    public void run() {

        List<Contest> allContests = contestRepository.getAll();
        List<User> allUsers = userRepository.getAll();
        List<Contest> allFinishedContests = contestRepository.getByPhase(CONTEST_PHASE_FINISHED);

        checkPhaseForContest(allContests);

        checkRank(allUsers);

        calculateAndRewardPoints(allFinishedContests);

        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:ms")) +
                "  Application task done successfully!");
    }


    public void checkPhaseForContest(List<Contest> contests) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        Date dateNow = new Date();
        dateFormat.format(dateNow);

        for (Contest contestItem : contests) {

            if (dateNow.after(contestItem.getPhase2FromHour())) {
                contestItem.setPhase(phaseRepository.getById(3));
            }

            if (dateNow.after(contestItem.getPhase2ToHour())) {
                contestItem.setPhase(phaseRepository.getById(4));
            }

            contestRepository.update(contestItem);
        }
    }

    public void checkRank(List<User> allUsers) {
        for (User user : allUsers) {
            if (user.getRanks().contains(rankRepository.getByName(RANK_NAME_MASTER))) {
                if (user.getScores() > 1000) {
                    user.setRanks(Set.of(rankRepository.getByName(RANK_NAME_PHOTO_DICTATOR)));
                    userRepository.update(user);
                    break;
                }
            }

            if (user.getRanks().contains(rankRepository.getByName(RANK_NAME_ENTHUSIAST))) {
                if (user.getScores() > 150) {
                    user.setRanks(Set.of(rankRepository.getByName(RANK_NAME_MASTER)));
                    userRepository.update(user);
                    break;
                }
            }

            if (user.getRanks().contains(rankRepository.getByName(RANK_NAME_JUNKIE))) {
                if (user.getScores() > 50) {
                    user.setRanks(Set.of(rankRepository.getByName(RANK_NAME_ENTHUSIAST)));
                    userRepository.update(user);
                }
            }
        }
    }

    public void calculateAndRewardPoints(List<Contest> finishedContests) {

        for (Contest contest : finishedContests) {

            if (!contest.getAssessed()) {

                Set<Image> images = contest.getImages();

                List<ImageRankingDto> imagesToBeAwarded = getImagesWithReviewedPoints(images);

                List<ImageRankingDto> firstPlace = new ArrayList<>();
                List<ImageRankingDto> secondPlace = new ArrayList<>();
                List<ImageRankingDto> thirdPlace = new ArrayList<>();
                int positionForAwardPoints = 1;

                boolean ifFirstWithDoubleScoreThenSecond = false;

                if (imagesToBeAwarded.size() > 1) {

                    for (int i = 0; i < imagesToBeAwarded.size() - 1; i++) {
                        var current = imagesToBeAwarded.get(i);
                        var next = imagesToBeAwarded.get(i + 1);

                        if (positionForAwardPoints > 3) break;

                        switch (positionForAwardPoints) {

                            case 1:
                                // FOR FIRST PLACE!
                                if (current.getPoints() == next.getPoints()) {
                                    firstPlace.add(current);
                                } else if (current.getPoints() > next.getPoints()) {
                                    if (current.getPoints() >= (next.getPoints() * 2)) {
                                        ifFirstWithDoubleScoreThenSecond = true;
                                    }
                                    firstPlace.add(current);
                                    positionForAwardPoints++;
                                }
                                break;

                            case 2:
                                // FOR SECOND PLACE!
                                if (current.getPoints() == next.getPoints()) {
                                    secondPlace.add(current);
                                    if (imagesToBeAwarded.size() == i + 2) {
                                        secondPlace.add(next);
                                    }
                                } else if (current.getPoints() > next.getPoints()) {
                                    secondPlace.add(current);
                                    if (imagesToBeAwarded.size() == i + 2) {
                                        thirdPlace.add(next);
                                    }
                                    positionForAwardPoints++;
                                }
                                break;

                            case 3:
                                // FOR THIRD PLACE!
                                if (current.getPoints() == next.getPoints()) {
                                    thirdPlace.add(current);
                                    if (imagesToBeAwarded.size() == i + 2) {
                                        thirdPlace.add(next);
                                    }
                                } else if (current.getPoints() > next.getPoints()) {
                                    thirdPlace.add(current);
                                    positionForAwardPoints++;
                                }
                                break;
                        }
                    }
                } else if (imagesToBeAwarded.size() == 1) {
                    firstPlace.add(imagesToBeAwarded.get(0));
                }

                awardFirstPlace(firstPlace, ifFirstWithDoubleScoreThenSecond);
                awardSecondAndThirdPlace(secondPlace, AWARD_POINTS_FOR_SECOND_PLACE);
                awardSecondAndThirdPlace(thirdPlace, AWARD_POINTS_FOR_THIRD_PLACE);
                contest.setAssessed(true);
                contestRepository.update(contest);
            }
        }
    }

    @NotNull
    private List<ImageRankingDto> getImagesWithReviewedPoints(Set<Image> images) {
        List<ImageRankingDto> imagesToBeAwarded = new ArrayList<>();

        for (Image image : images) {
            ImageRankingDto imageRankingDto = new ImageRankingDto();

            long pointsToSet = Math.max(image.getImageReviewsPoints(), DEFAULT_SCORE_FOR_NOT_REVIEWED_PHOTO);

            imageRankingDto.setImage(image.getId());
            imageRankingDto.setPoints(pointsToSet);

            imagesToBeAwarded.add(imageRankingDto);
        }

        imagesToBeAwarded.sort(Comparator.comparing(ImageRankingDto::getPoints).reversed());
        return imagesToBeAwarded;
    }

    private void awardFirstPlace(List<ImageRankingDto> firstPlace, boolean shouldAwardDoublePoints) {
        int pointsForFirstPosition = AWARD_POINTS_FOR_FIRST_PLACE;

        if (shouldAwardDoublePoints) {
            pointsForFirstPosition += AWARD_DOUBLE_POINTS_FOR_FIRST_POSITION;
        }

        if (firstPlace.size() > 1) {
            pointsForFirstPosition -= 10;
        }

        for (int i = 0; i < firstPlace.size(); i++) {
            Image image = imageRepository.getById(firstPlace.get(i).getImage());
            User user = image.getImageUser();
            int points = user.getScores() + pointsForFirstPosition;
            image.setWinnerPlace(1);
            user.setScores(points);
            imageRepository.update(image);
            userRepository.update(user);
        }
    }

    private void awardSecondAndThirdPlace(List<ImageRankingDto> placeRank, int awardPoints) {
        int pointsForThisPosition = awardPoints;

        if (placeRank.size() > 1) {
            pointsForThisPosition -= 10;
        }

        for (int i = 0; i < placeRank.size(); i++) {
            Image image = imageRepository.getById(placeRank.get(i).getImage());
            User user = image.getImageUser();
            int points = user.getScores() + pointsForThisPosition;
            if (awardPoints == AWARD_POINTS_FOR_SECOND_PLACE) {
                image.setWinnerPlace(2);
            } else {
                image.setWinnerPlace(3);
            }
            user.setScores(points);
            imageRepository.update(image);
            userRepository.update(user);
        }
    }
}
