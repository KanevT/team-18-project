package com.photocontest.utils;

public class Constants {

    // Error message
    public static final String INVALID_EMAIL = "Invalid email.";
    public static final String ENTITY_NOT_FOUND = "%s with %s %s not found.";
    public static final String USER_IS_NOT_ADMIN = "User is not admin and there are no rights under the respective action";
    public static final String USER_IS_NOT_JUNKIE = "User is not junkie and there are no rights under the respective action";
    public static final String USER_IS_NOT_PHOTO_DICTATOR = "User is not photo dictator and there are no rights under the respective action";
    public static final String USER_IS_NOT_ENTHUSIAST = "User is not enthusiast and there are no rights under the respective action";
    public static final String USER_IS_ANONYMOUS = "User is anonymous";
    public static final String DUPLICATE_ENTITY_ERROR = "%s with %s %s already exists.";
    public static final String WRONG_CONFIRMATION_PASSWORD = "Wrong username or password.";

    // Standard message
    public static final String RESOURCE_REQUIRES_AUTHENTICATION = "The requested resource requires authentication.";
    public static final String CUSTOMERS_COUNT = "Application have %d customers.";
    public static final String USER_FIRST_NAME_NULL_MESSAGE = "User first name can't be null";
    public static final String USER_NAME_SYMBOLS_MESSAGE = "User name should be between 2 and 15 symbols";
    public static final String PASSWORD_SYMBOLS_MESSAGE = "Password should have at least 8 characters";
    public static final String USER_LAST_NULL_MESSAGE = "User last name can't be null";
    public static final String EMAIL_CAN_NULL_MESSAGE = "Email can't be null";
    public static final String USERS_VISIBLE_DATA_MESSAGE = "Users can only see their own data!";
    public static final String MODIFY_USERS_MESSAGE = "Only creator or admin can modify users.";
    public static final String CATEGORY_NULL_MESSAGE = "Category can't be null";
    public static final String CATEGORY_NAME_MESSAGE = "Category name should be between 2 and 30 symbols";
    public static final String CONTEST_DATE_NULL_MESSAGE = "Contest start date can't be null";
    public static final String CONTEST_DATE_POSITIVE_MESSAGE = "Contest date should be positive";
    public static final String PHASE_ID_NULL_MESSAGE = "Phase ID can't be null.";
    public static final String PHASE_TITLE_NULL_MESSAGE = "Title can't be null and should be unique.";
    public static final String PHASE_ID_POSITIVE_MESSAGE = "Phase ID should be positive";
    public static final String CATEGORY_ID_NULL_MESSAGE = "Category ID can't be null.";
    public static final String CATEGORY_ID_POSITIVE_MESSAGE = "Category ID should be positive";
    public static final String IMAGE_TITLE_NULL_MESSAGE = "Image title can't be null.";
    public static final String IMAGE_TITLE_MESSAGE = "Image title should be between 2 and 50 symbols.";
    public static final String IMAGE_STORY_NULL_MESSAGE = "Image story can't be null.";
    public static final String IMAGE_STORY_MESSAGE = "Image title should be minimum 10 symbols.";
    public static final String IMAGE_URL_NULL_MESSAGE = "Image URL can't be null.";
    public static final String PHASE_NAME_NULL_MESSAGE = "Phase name can't be null.";
    public static final String PHASE_NAME_MESSAGE = "Phase name should be between 2 and 20 symbols.";
    public static final String RANG_NAME_NULL_MESSAGE = "Rang name can't be null.";
    public static final String RANG_NAME_MESSAGE = "Rang name should be between 2 and 15 symbols.";

    // Pages
    public static final String LOGIN_PAGE = "login";
    public static final String ERROR_NO_RIGHT = "Error! You have no right to see this contests";
    public static final String HOME_PAGE = "index";
    public static final String ABOUT_US_PAGE = "about-us";
    public static final String NOT_FOUND_PAGE = "not-found";
    public static final String NOT_AUTHORIZED_PAGE = "not-authorized";
    public static final String SHOW_ADMIN_PROFILE_PAGE = "profile-user";
    public static final String SHOW_CUSTOMER_PROFILE_PAGE = "profile-user-customer";
    public static final String CURRENT_PAGE_USERS = "currentPageUsers";
    public static final String CURRENT_PAGE_CONTESTS = "currentPageContests";
    public static final String USER_PAGE = "userPage";
    public static final String CONTEST_PAGE = "contestPage";
    public static final String UPDATE_CONTEST_PAGE = "contest-update";
    public static final String CREATE_CONTEST_PAGE = "contest-new";
    public static final String REGISTERED_USER_TO_CONTEST_PAGE = "registered-user-to-contest";
    public static final String ENROL_FAILURE_PAGE = "enroll-failure";


    // Other constants
    public static final String CURRENT_USER = "currentUser";
    public static final int START_PAGE_PAGINATION = 1;
    public static final int NUMBER_OF_ITEMS_PER_PAGE_PAGINATION = 5;
    public static final String USER_FILTER_SESSION = "userFilterSession";
    public static final String CONTEST_FILTER_SESSION = "contestFilterSession";
    public static final String LOGGED_USER = "loggedUser";
    public static final String ALL_CONTESTS = "allContests";
    public static final String ALL_USERS = "allUsers";
    public static final String CATEGORIES = "categories";
    public static final String PHASES = "phases";
    public static final String RANKS = "ranks";
    public static final String USER_FILTER_DTO = "userFilterDto";
    public static final String CONTEST_FILTER_DTO = "contestFilterDto";
    public static final String UPDATE_USER_DTO = "updateUserDto";
    public static final String UPDATE_CONTEST_DTO = "updateContestDto";
    public static final String PAGE_NUMBER_USERS = "pageNumberUsers";
    public static final String PAGE_NUMBER_CONTESTS = "pageNumberContests";
    public static final String ERROR = "error";
    public static final String POINT_IMAGE_POSITIVE_MESSAGE = "Point must be positive";
    public static final String CONTEST_TYPE_INVITATIONAL = "Invitational";
    public static final String CONTEST_TYPE_OPEN = "Open";
    public static final String THE_CATEGORY_IS_WRONG = "The category of the photo doesn't match the category of the contest.";
    public static final String ALREADY_RATE_PHOTO = "Already rate this photo, try with another one.";
    public static final int POINT_FOR_JOIN_OPEN_CONTEST = 1;
    public static final int POINT_FOR_JOIN_INVITATIONAL_CONTEST = 3;
    public static final String RANK_NAME_MASTER = "Master";
    public static final String RANK_NAME_PHOTO_DICTATOR = "Photo Dictator";
    public static final String RANK_NAME_ENTHUSIAST = "Enthusiast";
    public static final String RANK_NAME_JUNKIE = "Junkie";
    public static final int DEFAULT_SCORE_FOR_NOT_REVIEWED_PHOTO = 3;
    public static final int AWARD_POINTS_FOR_SECOND_PLACE = 35;
    public static final int AWARD_POINTS_FOR_THIRD_PLACE = 20;
    public static final int AWARD_POINTS_FOR_FIRST_PLACE = 50;
    public static final String CONTEST_PHASE_FINISHED = "Finished";
    public static final String ADMIN_RANK_USER = "Admin";
    public static final String USER_CANNOT_BE_ADDED_MESSAGE = "User cannot be added to jury! Choose some who is admin or photo dictator.";
    public static final String THE_START_OF_PHASE_ONE_ERROR_MESSAGE = "The start of phase one must be before the end of phase one";
    public static final String THE_START_OF_PHASE_TWO_ERROR_MESSAGE = "The start of phase two must be before the end of phase two";
    public static final String CONTEST_WITH_ID_D_CONTAINS_IMAGES_ERROR_MESSAGE = "Contest with id %d, contains images and cannot be deleted!";
    public static final String CONTEST_PHASE_1 = "Phase 1";
    public static final String PHASE_2 = "Phase 2";
    public static final String TITLE = "title";
    public static final String PHASE = "Phase";
    public static final String NAME = "name";
    public static final String HOURS = "hours";
    public static final String DAYS = "days";
    public static final String CONTEST = "Contest";
    public static final String WRONG_USERNAME_OR_PASSWORD_MESSAGE = "Wrong username or password.";
    public static final String CURRENT_USERNAME_MESSAGE = "currentUsername";
    public static final String NO_LOGGED_IN_USER_MESSAGE = "No logged in user";
}
