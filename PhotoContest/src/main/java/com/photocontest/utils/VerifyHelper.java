package com.photocontest.utils;

import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.Contest;
import com.photocontest.models.Image;
import com.photocontest.models.User;

import java.util.List;
import java.util.Set;

public class VerifyHelper {

    public static void verifyIfAdmin(User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(Constants.USER_IS_NOT_ADMIN);
        }
    }

    public static void verifyIfJunkie(User user) {
        if (!user.isJunkie()) {
            throw new UnauthorizedOperationException(Constants.USER_IS_NOT_JUNKIE);
        }
    }

    public static void verifyIfEnthusiast(User user) {
        if (!user.isEnthusiast()) {
            throw new UnauthorizedOperationException(Constants.USER_IS_NOT_ENTHUSIAST);
        }
    }

    public static void verifyIfPhotoDictator(User user) {
        if (!user.isPhotoDictator()) {
            throw new UnauthorizedOperationException(Constants.USER_IS_NOT_PHOTO_DICTATOR);
        }
    }

    public static void verifyIfAnonymous(User user) {
        if (!user.isJunkie() && !user.isEnthusiast()
                && !user.isMaster() && !user.isPhotoDictator() && !user.isAdmin()) {
            throw new UnauthorizedOperationException(Constants.USER_IS_ANONYMOUS);
        }
    }
}
