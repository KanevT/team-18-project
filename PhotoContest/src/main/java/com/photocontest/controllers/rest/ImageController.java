package com.photocontest.controllers.rest;

import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.mappers.ImageModelMapper;
import com.photocontest.models.DTO.ImageDto;
import com.photocontest.models.Image;
import com.photocontest.models.User;
import com.photocontest.services.contracts.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/images")
public class ImageController {

    private final ImageService imageService;
    private final ImageModelMapper modelMapper;

    @Autowired
    public ImageController(ImageService imageService, ImageModelMapper modelMapper) {
        this.imageService = imageService;
        this.modelMapper = modelMapper;
    }


    @GetMapping
    public List<Image> getAll() {
        return imageService.getAll();
    }

    @GetMapping("/{id}")
    public Image getById(@PathVariable int id) {
        try {
            return imageService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Image create(@Valid @RequestBody ImageDto imageDto) {
        try {

          Image imageToCreate= modelMapper.fromDto(imageDto);
            imageService.create(imageToCreate);
            return imageToCreate;

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @GetMapping("/search")
    public List<Image> getByTitle(@RequestParam(required = false) Optional<String> title,
                                  @RequestParam(required = false) Optional<String> story) {
        try {
            return imageService.searchByMultiply(title, story);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/winner")
    public List<Image> getWinnerImages() {
        try {
            return imageService.getWinnerImages();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
