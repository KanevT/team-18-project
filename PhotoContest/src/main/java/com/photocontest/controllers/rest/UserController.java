package com.photocontest.controllers.rest;

import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.mappers.UserModelMapper;
import com.photocontest.models.Contest;
import com.photocontest.models.DTO.UserDto;
import com.photocontest.models.User;
import com.photocontest.services.contracts.UserService;
import com.photocontest.utils.AuthenticationHelper;
import com.photocontest.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("api/users")
public class UserController {

    private final UserService service;
    private final UserModelMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService service, UserModelMapper mapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping("/count")
    public String getCustomerCount() {

        return String.format(Constants.CUSTOMERS_COUNT, service.getCustomerCount());
    }

    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {

        try {
            User users = authenticationHelper.tryGetUser(headers);
            return service.getAll(users);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);

            return service.getById(id, user);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PostMapping
    public User create(@Valid @RequestBody UserDto userDto) {

        try {

            User userToCreate = mapper.fromDto(userDto);
            service.create(userToCreate);
            return userToCreate;

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserDto userDto) {

        User user = authenticationHelper.tryGetUser(headers);


        try {
            User userToUpdate = mapper.fromDto(userDto, id);

            service.update(user, userToUpdate);

            return userToUpdate;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException d) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, d.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {

            User user = authenticationHelper.tryGetUser(headers);

            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/search")
    public List<User> searchByMultiple(@RequestParam(required = false) Optional<String> firstName,
                                       @RequestParam(required = false) Optional<String> lastName,
                                       @RequestParam(required = false) Optional<String> email,
                                       @RequestParam(required = false) Optional<String> username,
                                       @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);

            return service.searchByMultiple(firstName, lastName, email, username, user);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/word")
    public List<User> searchByOneWord(@RequestHeader HttpHeaders headers,
                                      @RequestParam(required = false) Optional<String> search) {

        try {
            User user = authenticationHelper.tryGetUser(headers);

            return service.searchByOneWord(search, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/contests")
    public Set<Contest> getUserContests(@PathVariable int id, @RequestHeader HttpHeaders headers) {

        try {

            User user = authenticationHelper.tryGetUser(headers);

            return service.getUserContests(id, user);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/scores")
    public int getScore(@RequestHeader HttpHeaders headers, @Valid @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);

        User currentUser = service.getById(id, user);

        return currentUser.getScores();
    }
}
