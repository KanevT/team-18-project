package com.photocontest.controllers.rest;

import com.photocontest.exceptions.*;
import com.photocontest.mappers.ContestModelMapper;
import com.photocontest.mappers.ImageModelMapper;
import com.photocontest.models.Contest;
import com.photocontest.models.DTO.ContestDto;
import com.photocontest.models.DTO.ImageDto;
import com.photocontest.models.Image;
import com.photocontest.models.User;
import com.photocontest.services.contracts.ContestService;
import com.photocontest.services.contracts.ImageService;
import com.photocontest.services.contracts.RankService;
import com.photocontest.services.contracts.UserService;
import com.photocontest.utils.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/contests")
public class ContestController {

    private final ContestService contestService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestModelMapper contestModelMapper;
    private final ImageService imageService;
    private final ImageModelMapper imageModelMapper;
    private final UserService userService;
    private final RankService rankService;

    @Autowired
    public ContestController(ContestService contestService,
                             AuthenticationHelper authenticationHelper,
                             ContestModelMapper contestModelMapper,
                             ImageService imageService,
                             ImageModelMapper imageModelMapper,
                             UserService userService,
                             RankService rankService) {
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
        this.contestModelMapper = contestModelMapper;
        this.imageService = imageService;
        this.imageModelMapper = imageModelMapper;
        this.userService = userService;
        this.rankService = rankService;
    }

    @GetMapping
    public List<Contest> getAll(@RequestHeader HttpHeaders headers) {

        try {
            User users = authenticationHelper.tryGetUser(headers);
            return contestService.getAll(users);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Contest getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);

            return contestService.getById(id, user);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/phase/search")
    public List<Contest> getByPhase(@RequestHeader HttpHeaders headers, @RequestParam(required = false) Optional<String> name) {
        try {
            User user = authenticationHelper.tryGetUser(headers);

            return contestService.getByPhase(name);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PostMapping(consumes = {"multipart/form-data", "application/json"})
    public Contest create(@RequestHeader HttpHeaders headers, @Valid @RequestPart("dto") ContestDto contestDto,
                          @RequestParam(name = "file") Optional<MultipartFile> file) {

        try {
            User user = authenticationHelper.tryGetUser(headers);

            String inputImage = "";

            if (file.isPresent()) {
                inputImage = Base64.getEncoder().encodeToString(file.get().getBytes());
            }

            Contest contestToCreate = contestModelMapper.fromDto(contestDto, contestDto.getType());


            if (!inputImage.isEmpty()) {
                String urlImage = imageService.uploadImageToImgurAPI(inputImage);
                imageModelMapper.setCoverPhotoLocal(contestToCreate, urlImage);
            }

            contestService.create(contestToCreate, user);
            return contestToCreate;

        } catch (DuplicateEntityException | ParseException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Contest update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody ContestDto contestDto) {

        User user = authenticationHelper.tryGetUser(headers);


        try {
            Contest contestToUpdate = contestModelMapper.fromDto(contestDto, id, "Open");

            contestService.update(contestToUpdate, user);

            return contestToUpdate;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException | ParseException d) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, d.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {

            User user = authenticationHelper.tryGetUser(headers);

            contestService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        } catch (RuleDeleteException r) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, r.getMessage());
        }
    }

    @GetMapping("/{id}/users")
    Set<User> viewInUsers(@RequestHeader HttpHeaders headers, @PathVariable int id) {

        try {
            User user = authenticationHelper.tryGetUser(headers);

            Contest contest = contestService.getById(id, user);

            return contestService.getContestUserSet(user, contest);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/{id}/jury")
    Set<User> viewInJury(@RequestHeader HttpHeaders headers, @PathVariable int id) {

        try {
            User user = authenticationHelper.tryGetUser(headers);

            Contest contest = contestService.getById(id, user);

            return contestService.getContestJurySet(user, contest);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PostMapping("/{contestId}/addUser/{userId}")
    public User addUserToContest(@RequestHeader HttpHeaders headers,
                                 @PathVariable int contestId,
                                 @PathVariable int userId) {
        try {

            User user = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(contestId, user);
            User userToAdd = userService.getById(userId, user);

            return contestService.addUserToContest(userToAdd, contest);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }


    @PostMapping(value = {"/{id}/upload/photo"}, consumes = {"multipart/form-data", "application/json"})
    Contest addPhoto(@RequestHeader HttpHeaders headers,
                     @Valid @RequestPart("imageDto") ImageDto imageDto,
                     @RequestParam(name = "file") Optional<MultipartFile> file,
                     @PathVariable int id) {

        try {
            User user = authenticationHelper.tryGetUser(headers);

            Contest contest = contestService.getById(id, user);

            String inputImage = "";

            if (file.isPresent()) {
                inputImage = Base64.getEncoder().encodeToString(file.get().getBytes());
            }

            Image image = imageModelMapper.fromDto(imageDto);


            if (!inputImage.isEmpty()) {
                String urlImage = imageService.uploadImageToImgurAPI(inputImage);
                image.setImageUrl(urlImage);
            }

            imageService.create(image);

            contestService.addImageToContest(contest, image);

            userService.addImageToUser(user, image);

            contestService.update(contest, user);
            return contest;
        } catch (EntityNotFoundException | IOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PostMapping("/{contestId}/addJury/{userId}")
    public User addJuryToContest(@RequestHeader HttpHeaders headers,
                                 @PathVariable int contestId,
                                 @PathVariable int userId) {
        try {

            User user = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(contestId, user);
            User userToAdd = userService.getById(userId, user);

            contestService.checkIfCanBeJury(userToAdd);

            return contestService.addJuryToContest(userToAdd, contest);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        } catch (InvalidDtoDataException i) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, i.getMessage());
        }
    }
}
