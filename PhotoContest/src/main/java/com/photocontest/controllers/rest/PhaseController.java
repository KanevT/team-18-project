package com.photocontest.controllers.rest;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.mappers.PhaseModelMapper;
import com.photocontest.models.Phase;
import com.photocontest.services.contracts.PhaseService;
import com.photocontest.utils.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/phases")
public class PhaseController {

    private final PhaseService phaseService;
    private final PhaseModelMapper phaseModelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PhaseController(PhaseService phaseService,
                           PhaseModelMapper phaseModelMapper,
                           AuthenticationHelper authenticationHelper) {
        this.phaseService = phaseService;
        this.phaseModelMapper = phaseModelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Phase> getAll() {
        return phaseService.getAll();
    }

    @GetMapping("/{id}")
    public Phase getById(@PathVariable int id) {
        try {
            return phaseService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public Phase getByName(@RequestParam(required = false) String name) {
        try {
            return phaseService.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
