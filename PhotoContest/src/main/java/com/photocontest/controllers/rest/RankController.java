package com.photocontest.controllers.rest;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Rank;
import com.photocontest.services.contracts.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/ranks")
public class RankController {

    private final RankService rankService;

    @Autowired
    public RankController(RankService rankService) {
        this.rankService = rankService;
    }

    @GetMapping
    public List<Rank> getAll() {
        return rankService.getAll();
    }

    @GetMapping("/{id}")
    public Rank getById(@PathVariable int id) {
        try {
            return rankService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public Rank getByName(@RequestParam(required = false) Optional<String> name) {
        try {
            return rankService.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
