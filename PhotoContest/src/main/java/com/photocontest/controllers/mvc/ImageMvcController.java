package com.photocontest.controllers.mvc;

import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.Category;
import com.photocontest.models.Rank;
import com.photocontest.models.User;
import com.photocontest.services.contracts.CategoryService;
import com.photocontest.services.contracts.ContestService;
import com.photocontest.services.contracts.ImageService;
import com.photocontest.services.contracts.PhaseService;
import com.photocontest.utils.AuthenticationHelper;
import com.photocontest.utils.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/images")
public class ImageMvcController {

    private final ImageService imageService;
    private final AuthenticationHelper authenticationHelper;
    private final CategoryService categoryService;
    private final PhaseService phaseService;
    private final ContestService contestService;


    public ImageMvcController(ImageService imageService,
                              AuthenticationHelper authenticationHelper,
                              CategoryService categoryService,
                              PhaseService phaseService,
                              ContestService contestService) {
        this.imageService = imageService;
        this.authenticationHelper = authenticationHelper;
        this.categoryService = categoryService;
        this.phaseService = phaseService;
        this.contestService = contestService;
    }


    @GetMapping
    public String showAllImages(Model model, HttpSession session) {

        model.addAttribute("images", imageService.getAll());
        model.addAttribute("contests", contestService.getAll(getUser()));
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute(Constants.CURRENT_USER, currentUser);

        } catch (UnauthorizedOperationException e) {
            model.addAttribute(Constants.CURRENT_USER, null);
        }
        return "images";
    }


    private User getUser() {
        User user = new User("testUser", "1234", "Ivan",
                "Ivanov", "ivanov@yahoo.com", 0);
        user.setRanks(Set.of(new Rank(1, "Admin")));
        return user;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }


}
