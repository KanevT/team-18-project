package com.photocontest.controllers.mvc;

import com.photocontest.exceptions.AuthorisationException;
import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.mappers.RegisterMapper;
import com.photocontest.mappers.UserModelMapper;
import com.photocontest.models.DTO.DeleteUserDto;
import com.photocontest.models.DTO.UpdatePasswordDto;
import com.photocontest.models.DTO.UpdateUserDto;
import com.photocontest.models.User;
import com.photocontest.services.contracts.ImageService;
import com.photocontest.services.contracts.UserService;
import com.photocontest.utils.AuthenticationHelper;
import com.photocontest.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Base64;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;
    private final RegisterMapper registerMapper;
    private final ImageService imageService;

    @Autowired
    public UserMvcController(UserService userService,
                             AuthenticationHelper authenticationHelper,
                             UserModelMapper userModelMapper,
                             RegisterMapper registerMapper,
                             ImageService imageService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
        this.registerMapper = registerMapper;
        this.imageService = imageService;
    }

    @GetMapping("/{id}")
    public String showUserProfile(@PathVariable int id, Model model, HttpSession session) {
        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("isPersonal", true);
            model.addAttribute("activeContest", userService.getUserActiveContests(currentUser));
            model.addAttribute("finishedContests", userService.getUserFinishedContests(currentUser));
            model.addAttribute("uploadPictures", currentUser.getImages());

            model.addAttribute("updateUserDto", new UpdateUserDto());
            model.addAttribute("updatePasswordDto", new UpdatePasswordDto());
            model.addAttribute("deleteUserDto", new DeleteUserDto());


            User user = userService.getById(id, currentUser);
            model.addAttribute("userView", user);

            if (currentUser.isAdmin()) {
                return Constants.SHOW_ADMIN_PROFILE_PAGE;
            }

            return "profile-user";
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }

    @PostMapping("/{id}/update")
    public String handleUpdate(@PathVariable int id, Model model,
                               @Valid @ModelAttribute("updateDto") UpdateUserDto updateUserDto,
                               @RequestParam("fileImage") MultipartFile multipartFile,
                               BindingResult bindingResult, HttpSession session) throws IOException {

        if (bindingResult.hasErrors()) {
            return "redirect:users/{id}/update";
        }

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            String inputImage = Base64.getEncoder().encodeToString(multipartFile.getBytes());

            User inputUser = userService.getById(id, currentUser);
            User userToUpdate = registerMapper.updateMethod(updateUserDto, new User());
            userToUpdate.setId(inputUser.getId());
            userToUpdate.setPassword(inputUser.getPassword());
            User outputUser = userModelMapper.inversion(inputUser, userToUpdate);

            if (!inputImage.isEmpty()) {
                String urlImage = imageService.uploadImageToImgurAPI(inputImage);
                outputUser.setPicture(urlImage);
            }

            userService.update(currentUser, outputUser);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }

        return "redirect:/users/{id}";
    }

    @PostMapping("/{id}/update/password")
    public String handlePasswordUpdate(@PathVariable int id,
                                       Model model,
                                       @ModelAttribute UpdatePasswordDto updateData,
                                       BindingResult bindingResult,
                                       HttpSession session) {

        if (bindingResult.hasErrors())
            bindingResult.rejectValue("password", "password_error", "Password don't match");

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            User inputUser = userService.getById(id, currentUser);

            userService.passwordUpdate(updateData, inputUser);

        } catch (EntityNotFoundException e) {
            return "redirect:/";
        }
        return "redirect:/users/{id}";
    }

    @PostMapping("/{id}/profile/delete")
    public String deleteProfile(@PathVariable int id,
                                Model model,
                                @ModelAttribute DeleteUserDto dto,
                                HttpSession session) {

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            userService.delete(id, currentUser);

        } catch (EntityNotFoundException | AuthorisationException e) {
            return "redirect:/users/{id}";
        }
        return "redirect:/logout";
    }
}
