package com.photocontest.controllers.mvc;

import com.photocontest.exceptions.*;
import com.photocontest.mappers.ContestModelMapper;
import com.photocontest.mappers.ImageModelMapper;
import com.photocontest.mappers.ReviewModelMapper;
import com.photocontest.mappers.UpdateContestModelMapper;
import com.photocontest.models.*;
import com.photocontest.models.DTO.ContestDto;
import com.photocontest.models.DTO.ImageDto;
import com.photocontest.models.DTO.RateDto;
import com.photocontest.models.DTO.UpdateContestDto;
import com.photocontest.services.contracts.*;
import com.photocontest.utils.AuthenticationHelper;
import com.photocontest.utils.Constants;
import com.photocontest.utils.VerifyHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.util.Base64;
import java.util.List;
import java.util.Set;

import static com.photocontest.utils.Constants.CONTEST_TYPE_OPEN;

@Controller
@RequestMapping("/contests")
public class ContestMvcController {

    private final ContestService contestService;
    private final AuthenticationHelper authenticationHelper;
    private final CategoryService categoryService;
    private final ContestModelMapper contestModelMapper;
    private final UpdateContestModelMapper updateContestModelMapper;
    private final PhaseService phaseService;
    private final ImageService imageService;
    private final ImageModelMapper imageModelMapper;
    private final UserService userService;
    private final ReviewModelMapper reviewModelMapper;
    private final ReviewService reviewService;

    public ContestMvcController(ContestService contestService, ImageService imageService,
                                AuthenticationHelper authenticationHelper,
                                UpdateContestModelMapper updateContestModelMapper,
                                ContestModelMapper contestModelMapper,
                                CategoryService categoryService, PhaseService phaseService,
                                ImageModelMapper imageModelMapper, UserService userService,
                                ReviewModelMapper reviewModelMapper, ReviewService reviewService) {
        this.updateContestModelMapper = updateContestModelMapper;
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
        this.categoryService = categoryService;
        this.contestModelMapper = contestModelMapper;
        this.imageModelMapper = imageModelMapper;
        this.phaseService = phaseService;
        this.imageService = imageService;
        this.userService = userService;
        this.reviewModelMapper = reviewModelMapper;
        this.reviewService = reviewService;
    }

    @GetMapping
    public String showAllContests(Model model, HttpSession session) {

        model.addAttribute("contests", contestService.getAll(getUser()));

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute(Constants.CURRENT_USER, currentUser);

        } catch (UnauthorizedOperationException e) {
            model.addAttribute(Constants.CURRENT_USER, null);
        }
        return "contests";
    }

    @GetMapping("/{id}")
    public String showContest(@PathVariable int id, Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute(Constants.CURRENT_USER, currentUser);
            Contest contest = contestService.getById(id, currentUser);
            model.addAttribute("currentContest", contest);
            model.addAttribute("getImageReviewsUsers", contestService.getImageReviewsUsers(contest, currentUser));
            model.addAttribute("checkIfCurrentUserAlreadyUploadPhoto", contestService.checkIfUserAlreadyUploadPhoto(contest, currentUser));
            model.addAttribute("rateDto", new RateDto());
            model.addAttribute("imageDto", new ImageDto());

            return "contest";
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return "not-authorized";
        }
    }

    @PostMapping("/{id}/update")
    public String updateContest(@PathVariable int id,
                                @Valid @ModelAttribute UpdateContestDto updateContestDto,
                                BindingResult error,
                                Model model, HttpSession session) {
        if (error.hasErrors()) {
            return "redirect:/admin";
        }
        try {
            User currentUser = authenticationHelper.tryGetUser(session);

            Contest contest = contestService.getById(id, currentUser);
            Contest contestToUpdate = updateContestModelMapper.dtoToObjectUpdate(updateContestDto, contest);

            contestService.update(contestToUpdate, currentUser);

            return "redirect:/admin";
        } catch (DuplicateEntityException e) {
            error.rejectValue("name", "contest.exist", e.getMessage());
            return Constants.UPDATE_CONTEST_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (ParseException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }


    @PostMapping("/new")
    public String createContest(@Valid @ModelAttribute("contestDto") ContestDto contestDto,
                                BindingResult errors,
                                @RequestParam("imageContest") MultipartFile multipartFile,
                                HttpSession session) {

        if (errors.hasErrors()) {
            return "redirect:/admin";
        }
        try {
            User currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIfAdmin(currentUser);

            String inputImage = Base64.getEncoder().encodeToString(multipartFile.getBytes());

            Contest newContest = contestModelMapper.fromDto(contestDto, CONTEST_TYPE_OPEN);

            if (!inputImage.isEmpty()) {
                String urlImage = imageService.uploadImageToImgurAPI(inputImage);
                imageModelMapper.setCoverPhotoLocal(newContest, urlImage);
            }

            contestService.create(newContest, currentUser);

            contestService.addJury(contestDto, newContest);

            contestService.update(newContest, currentUser);

            return "redirect:/admin";
        } catch (EntityNotFoundException | ParseException | IOException | InvalidTimePeriodException e) {
            return Constants.NOT_FOUND_PAGE;
        } catch (DuplicateEntityException e) {
            errors.rejectValue("title", "contest-exist", e.getMessage());
            return "redirect:/admin";
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @PostMapping("/new/invitational")
    public String createContestInvitational(@Valid @ModelAttribute("contestDto") ContestDto contestDto,
                                            BindingResult errors,
                                            @RequestParam("imageContestInvitational") MultipartFile multipartFile,
                                            HttpSession session) {

        if (errors.hasErrors()) {
            return "redirect:/admin";
        }

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            VerifyHelper.verifyIfAdmin(currentUser);

            String inputImage = Base64.getEncoder().encodeToString(multipartFile.getBytes());

            Contest newContest = contestModelMapper.fromDto(contestDto, Constants.CONTEST_TYPE_INVITATIONAL);

            if (!inputImage.isEmpty()) {
                String urlImage = imageService.uploadImageToImgurAPI(inputImage);
                imageModelMapper.setCoverPhotoLocal(newContest, urlImage);
            }

            contestService.create(newContest, currentUser);

            contestService.addJury(contestDto, newContest);

            contestService.addParticipants(contestDto, newContest);

            contestService.checkIfJuryUserIsSelectedToParticipants(newContest);

            contestService.update(newContest, currentUser);

            return "redirect:/admin";
        } catch (EntityNotFoundException | ParseException | IOException
                | InvalidTimePeriodException | DuplicateEntityException e) {
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteContest(@PathVariable int id, Model model, HttpSession session) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            VerifyHelper.verifyIfAdmin(currentUser);

            contestService.delete(id, currentUser);
            return "redirect:/contests";

        } catch (EntityNotFoundException | RuleDeleteException e) {
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @PostMapping("/{id}/enroll")
    public String enrollUserToContest(@Valid @ModelAttribute("imageDto") ImageDto imageDto,
                                      @PathVariable int id,
                                      @RequestParam("enrollImage") MultipartFile multipartFile,
                                      BindingResult errors,
                                      HttpSession session) {
        if (errors.hasErrors()) {
            return "redirect:/contests/{id}";
        }

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            Contest contest = contestService.getById(id, currentUser);
            VerifyHelper.verifyIfAnonymous(currentUser);

            String inputImage = Base64.getEncoder().encodeToString(multipartFile.getBytes());
            Image image = imageModelMapper.fromDto(imageDto);

            if (!inputImage.isEmpty()) {
                String urlImage = imageService.uploadImageToImgurAPI(inputImage);
                image.setImageUrl(urlImage);
            }

            imageService.create(image);

            if (contest.getType().getName().equals(CONTEST_TYPE_OPEN)) {
                contestService.addUserToContest(currentUser, contest);
            }

            contestService.addImageToContest(contest, image);
            userService.addImageToUser(currentUser, image);

            return "redirect:/contests/{id}";
        } catch (EntityNotFoundException | IOException e) {
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @PostMapping("/{contestId}/rateImage/{imageId}")
    public String rateContestImage(@Valid @ModelAttribute("rateDto") RateDto rateDto,
                                   @PathVariable int contestId,
                                   @PathVariable int imageId,
                                   BindingResult bindingResult,
                                   HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/contests/{contestId}";
        }

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            VerifyHelper.verifyIfAnonymous(currentUser);

            Contest contest = contestService.getById(contestId, currentUser);
            Image image = imageService.getById(imageId);

            contestService.checkIfUserIsRateImage(currentUser, image);

            Review review = reviewModelMapper.fromDto(rateDto, currentUser, rateDto.getComplain());

            reviewService.addReviewToImage(review, image);

            return "redirect:/contests/{contestId}";
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (ЕvaluationCheckPictureException e) {
            return "redirect:/contests/{contestId}";
        }
    }

    private User getUser() {
        User user = new User("testUser", "1234", "Ivan",
                "Ivanov", "ivanov@yahoo.com", 0);
        user.setRanks(Set.of(new Rank(1, "Admin")));
        return user;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("phases")
    public List<Phase> populatePhases() {
        return phaseService.getAll();
    }
}
