![alt](https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)

---

# PhotoContest
> PhotoContest is a web application enabling registered users to be able to participate in competitions related to the respective category, to upload photos which are subsequently evaluated by a jury, can accumulate points and raise the rank in the respective ranking.
---

[**Trello board**](https://trello.com/b/MBv4ccx0/photo-contest)
<br/>

---

> The data of the application is stored in a relational database. The application consumes one public REST services in order to achieve its main functionality - Imgur Service. Initial Seed and subsequent Sync of database is performed only via Admin profile.
> The uploaded photos in the last shot are kept only as a URL in the database, and the main photo is stored on the site of Imgur.com

![alt](https://i.imgur.com/TyWZvbM.png)
![alt](https://i.imgur.com/VyBjiry.png)
---
## Table of Contents

- [Installation](#installation)
- [Technologies](#technologies)
- [Features](#features)
- [Team](#team)

## Installation

### Prerequisites
The following list of software should be installed:
- [MariaDB](https://mariadb.org/)
- [IntelliJ IDEA](https://www.jetbrains.com/idea/)
- [Java SE Development Kit 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)

### Clone
- Clone the project using `https://gitlab.com/KanevT/team-18-project.git`

### Setup

- Open the project via IntelliJ IDEA
> restore the project and install all dependencies
```
going on build.gradle file then click reload
```
- Database
> updates the database to the last migration
```
1) Open the folder 'dbscripts' in team-18-project/PhotoContest/dbscripts.
2) Inside this folder have two files - one for create database and second for filling it.
```
- Run the project:
> click on run icon for run the project.
```
it may take some time to launch the application.
```
- Open https://localhost:8080 in your browser
> If you want to have full functionality over the admin account, you need to log in with the following data:
> 1. Login as Admin:
>  - Username: ibrahim
>  - Password: 1234
>
>  - Username: todor
>  - Password: 1234
>
> 2. Now you are admin, congratulations!

## Technologies

- Java
- Spring
- Gradle
- Postman
- Hibernate
- Thymeleaf
- MariaDb Server
- Swagger
- JUnit
- GitLab
- HTML
- CSS
- Bootstrap
- JavaScript
- AJAX

## Features

#### In the right part you can see a menu button, which lists any of the following pages: Register, Login, Contests, Gallery, About us.

#### On the home page you can see open competitions, the phase they are in at the moment and how much time is left until the next phase, you can also see photos that have already won previous competitions. If contest is already in phase two, you cannot upload file, but admins can rate them.

![alt](https://i.imgur.com/fPBkdsi.gif)
---
<br/><br/>

#### If you are a registered user, in your account you have information about all the contests that are active and participate and all those that have already finished, you can also view the photo gallery.

> 1. Login as User:
>  - Username: david
>  - Password: 1234

![alt](https://i.imgur.com/237a27G.gif)
---
<br/><br/>

#### You can also take part in one of the open contests and upload a photo (i.e. to participate in the contest)

![alt](https://i.imgur.com/EhfSFo8.gif)
---
<br/><br/>

#### If a competition is over, you can see how many points and what comments the participants received.

![alt](https://i.imgur.com/sOZPu10.gif)
---
<br/><br/>

#### The organizers have access to the admin page where they can see all registered users of their site, all contests that have been created, or create a new contest of the type "Open" or "By invitation"

![alt](https://i.imgur.com/kf41gGY.gif)
---
<br/><br/>

#### If the competition is in phase 2, the jury can evaluate photos in the competition and write comments. 

![alt](https://i.imgur.com/Ltxx1a9.gif)
---
<br/><br/>

## Documentation

#### API documentation is accessed only through authentication. Two options are available - via Identity or via JSON Web Token. Admin role is required to access Adm API Controller.
1. If you try to access any resources through Postman:
> Key: Authentication
> Value: ibrahim.izirov@gmail.com

![alt](https://i.imgur.com/WUcRjQx.gif)
---
<br/><br/>

## Team
* Todor Kunev - [GitLab](https://gitlab.com/KanevT)
* Ibrahim Izirov - [GitLab](https://gitlab.com/IbrahimIzirov)

